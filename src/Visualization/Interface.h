namespace Visualization
{

    class Interface : public SQL::Interface<false>
    {

    public:

        // static constexpr std::string MakeTableQuery{
        //     "CREATE TABLE IF NOT EXISTS {table} ({values});"
        // };
        // static constexpr std::string InsertQuery{
        //     "INSERT OR IGNORE INTO {table} VALUES {mask};"
        // };


        // static constexpr std::string UnitsColumns{
        //     "Type TEXT, Value REAL, Units TEST"
        // };
        // static constexpr std::string UnitsMask{ "(?1,?2,?3)" };


        // static constexpr std::string BasisColumns{
        //     "SubBIdx INTEGER PRIMARY KEY, m INTEGER, l INTEGER, k INTEGER"
        // };
        // static constexpr std::string BasisMask{ "(?1,?2,?3,?4)" };

        // static constexpr std::string ParamColumns{
        //     "???"
        // };

    // std::format("",std::to_string(n))

    // private:
    //     Container<nidxs()>* _ptr{ nullptr };

    // public:
    //     Interface() = default;
    //     ~Interface() { delete _ptr; }

    //     void Initialize(size_t lki1, size_t lki2)
    //     {
    //         if( _ptr == nullptr )
    //         {
    //             try{ ImportCoeffs(lki1,lki2); }
    //             catch (const char* e)
    //             {
    //                 std::cerr << "Could not initialize Coeff::Interface.\n";
    //                 std::cerr << "Error code: " << e << std::endl;
    //             }
    //         }
    //     }
    //     void ImportCoeffs(size_t lki1, size_t lki2)
    //     {

    //         rc = OpenDatabase(DBPath.c_str());

    //         // Prepare a statement to extract the coefficients from database.
    //         sqlite3_stmt* Stmt{ nullptr };
    //         rc = PrepareStatement(&Stmt, Query);
    //         if( rc != SQLITE_OK ) { EndProgram(); }

    //         int l1 = Ordered_lks[lki1][0];
    //         int k1 = Ordered_lks[lki1][1];
    //         int l2 = Ordered_lks[lki2][0];
    //         int k2 = Ordered_lks[lki2][1];

    //         // Bind specific values for each state to the sqlite query.
    //         rc = sqlite3_bind_int(Stmt,1,l1);
    //         if( rc != SQLITE_OK ) { EndProgram(); }

    //         rc = sqlite3_bind_int(Stmt,2,k1);
    //         if( rc != SQLITE_OK ) { EndProgram(); }

    //         rc = sqlite3_bind_int(Stmt,3,l2);
    //         if( rc != SQLITE_OK ) { EndProgram(); }

    //         rc = sqlite3_bind_int(Stmt,4,k2);
    //         if( rc != SQLITE_OK ) { EndProgram(); }

    //         // Fill allocated index and coefficients array.
    //         rc = sqlite3_step(Stmt);
    //         _ptr = new Container<nidxs()>;
    //         while (rc == SQLITE_ROW)
    //         {
    //             double c{ sqlite3_column_double(Stmt, nidxs()) };
    //             if constexpr( nidxs() == 0 ) { _ptr->push_back(c); }
    //             else
    //             {
    //                 std::array<int,nidxs()> ix;
    //                 for(unsigned int p = 0; p<nidxs(); p++) {
    //                     ix[p] = sqlite3_column_int(Stmt, p);
    //                 }
    //                 _ptr->push_back(ix,c);
    //             }
    //             rc = sqlite3_step(Stmt);
    //         }

    //         rc = FinalizeStatement(Stmt);
    //         rc = CloseDatabase();
    //     }

    //     int NumIndices() const { return _ptr->NumIndices(); }
    //     double value(size_t n) const { return _ptr->value(n); }
    //     int index(size_t n, unsigned int j) const { return _ptr->index(n,j); }
    //     size_t size() const { return _ptr->size(); }
    };
}