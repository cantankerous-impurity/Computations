namespace Visualization
{
    class Element
    {
        std::tuple<
            Pair<Kinetic::Spec::II,0,Decay::Type::Zero>,
            Pair<Kinetic::Spec::II,0,Decay::Type::Yukawa>,
            Pair<Kinetic::Spec::IK,0,Decay::Type::Zero>,
            Pair<Kinetic::Spec::IK,0,Decay::Type::Yukawa>,
            Pair<Kinetic::Spec::KI,0,Decay::Type::Zero>,
            Pair<Kinetic::Spec::KI,0,Decay::Type::Yukawa>,
            Pair<Kinetic::Spec::II,1,Decay::Type::Zero>,
            Pair<Kinetic::Spec::II,1,Decay::Type::Yukawa>,
            Pair<Kinetic::Spec::KK,0,Decay::Type::Zero>,
            Pair<Kinetic::Spec::KK,0,Decay::Type::Yukawa>,
            Pair<Kinetic::Spec::KI,1,Decay::Type::Zero>,
            Pair<Kinetic::Spec::KI,1,Decay::Type::Yukawa>,
            Pair<Kinetic::Spec::IK,1,Decay::Type::Zero>,
            Pair<Kinetic::Spec::IK,1,Decay::Type::Yukawa>,
            Pair<Kinetic::Spec::II,2,Decay::Type::Zero>,
            Pair<Kinetic::Spec::II,2,Decay::Type::Yukawa>
        > _data;
    public:
        Element() = default;

        template < Kinetic::Spec K, size_t ex, Decay::Type D >
        Pair<K,ex,D>& Get()
        {
            return std::get<Pair<K,ex,D>>(_data);
        }

        template < Kinetic::Spec K, size_t ex, Decay::Type D >
        const Pair<K,ex,D>& Get() const
        {
            return std::get<Pair<K,ex,D>>(_data);
        }

        template < Kinetic::Spec K, size_t ex, Decay::Type D, bool dZ >
        Interface<K,ex,D,dZ>& Get()
        {
            return Get<K,ex,D>().template Get<dZ>();
        }

        template < Kinetic::Spec K, size_t ex, Decay::Type D, bool dZ >
        const Interface<K,ex,D,dZ>& Get() const
        {
            return Get<K,ex,D>().template Get<dZ>();
        }

        template < Kinetic::Spec K, size_t ex, Decay::Type D, bool dZ >
        double value(size_t n) const
        {
            return Get<K,ex,D>().value<dZ>(n);
        }
        template < Kinetic::Spec K, size_t ex, Decay::Type D, bool dZ >
        int index(size_t n, int j) const
        {
            return Get<K,ex,D>().index<dZ>(n,j);
        }

        template< bool dZ >
        void Initialize(size_t lkN)
        {
            IndexPair iP { UnpackIdx(lkN) };
            Initialize<dZ>(iP[0],iP[1]);
        }
        template< bool dZ >
        void Initialize(size_t lki1,size_t lki2)
        {
            std::get<0>(_data).Initialize<dZ>(lki1,lki2);
            std::get<1>(_data).Initialize<dZ>(lki1,lki2);
            std::get<2>(_data).Initialize<dZ>(lki1,lki2);
            std::get<3>(_data).Initialize<dZ>(lki1,lki2);
            std::get<4>(_data).Initialize<dZ>(lki1,lki2);
            std::get<5>(_data).Initialize<dZ>(lki1,lki2);
            std::get<6>(_data).Initialize<dZ>(lki1,lki2);
            std::get<7>(_data).Initialize<dZ>(lki1,lki2);
            std::get<8>(_data).Initialize<dZ>(lki1,lki2);
            std::get<9>(_data).Initialize<dZ>(lki1,lki2);
            std::get<10>(_data).Initialize<dZ>(lki1,lki2);
            std::get<11>(_data).Initialize<dZ>(lki1,lki2);
            std::get<12>(_data).Initialize<dZ>(lki1,lki2);
            std::get<13>(_data).Initialize<dZ>(lki1,lki2);
            std::get<14>(_data).Initialize<dZ>(lki1,lki2);
            std::get<15>(_data).Initialize<dZ>(lki1,lki2);
        }
    };
}
