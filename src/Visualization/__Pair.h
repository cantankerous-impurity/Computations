namespace Visualization
{
    template < Kinetic::Spec K, size_t ex, Decay::Type D >
    class Pair
    {
    public:

        template< bool SR >
        using CI = Interface<K,ex,D,SR>;

    private:

        std::tuple<CI<false>,CI<true>> _data;

    public:

        Pair() = default;

        template< bool SR >
        CI<SR>& Get() { return std::get<CI<SR>>(_data); }
        template< bool SR >
        const CI<SR>& Get() const { return std::get<CI<SR>>(_data); }

        template< bool SR >
        void Initialize(size_t lki1, size_t lki2)
        {
            return Get<SR>().Initialize(lki1,lki2);
        }


        template< bool SR >
        int NumIndices() const { return Get<SR>().NumIndices(); }
        template< bool SR >
        double value(size_t n) const { return Get<SR>().value(n); }
        template< bool SR >
        int index(size_t n, int j) const { return Get<SR>().index(n,j); }
    };
}