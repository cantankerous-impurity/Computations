namespace Integrand
{

    using namespace Mesh;
    namespace SolidAngle
    {

        double Legendre(const double chi, const ui_t L, const ui_t m)
        {
            if (m>L ) { return 0.; }
            return boost::math::legendre_p(L,m,chi);
        }

        double Azimuthal(const double phi, const ui_t m, const Flip &Par)
        {
            switch (m)
            {
                case 0:
                    switch (Par)
                    {
                        case Flip::Heads:
                            return 1.;
                        case Flip::Tails:
                            return 0.;
                        default:
                            throw;
                    }
                default:
                    switch (Par)
                    {
                        case Flip::Heads:
                            return sqrt(2)*cos(m*phi);
                        case Flip::Tails:
                            return sqrt(2)*sin(m*phi);
                        default:
                            throw;
                    }
            }
        }

        template<
            Kinetic::Spec kS,
            Operator::Spec oS, Term::Type tT, Bloch::Type bT
        >
        struct KronPAB{};

        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
        struct KronPAB<Kinetic::Spec::KK,oS,tT,bT>
        {
            KinKin<double> operator()( Calcs<oS,tT,bT> &iC )
            {
                if (iC.sP.mL() != iC.sP.mR()) { throw; }
                auto l0{ iC.sP.l0() };
                auto l1{ iC.sP.l1() };
                if( (std::max(l0,l1) - std::min(l0,l1)) > 4 ) { throw; }

                std::array<double,3> KS0Arr { 0. }; Kin<double> KS0{ KS0Arr };
                std::array<double,3> KS1Arr { 0. }; Kin<double> KS1{ KS1Arr };
                KS0 = iC.sP.KS0();
                double && rE0{ iC.EffectiveRadScale(0) };
                if constexpr(WfType == Radial::Type::Slater)
                { KS0(-1) /= rE0; }
                if constexpr(WfType == Radial::Type::Gauss)
                { KS0(-1) /= rE0*rE0; }
                KS1 = iC.sP.KS1();
                double && rE1{ iC.EffectiveRadScale(1) };
                if constexpr(WfType == Radial::Type::Slater)
                { KS1(-1) /= rE1; }
                if constexpr(WfType == Radial::Type::Gauss)
                { KS1(-1) /= rE1*rE1; }

                KinKin<double> res{ iC.GetSAKinKin(), { 0. } };
                if(l0==l1) {
                    res(-1,-1) = KS0(-1)*KS1(-1);
                    res(0,0) = KS0(0)*KS1(0);
                    res(+1,+1) = KS0(+1)*KS1(+1);
                }
                if(l0==l1-2) {
                    res(0,-1) = KS0(0)*KS1(-1);
                    res(+1,0) = KS0(+1)*KS1(0);
                }
                if(l0==l1+2)
                {
                    res(-1,0) = KS0(-1)*KS1(0);
                    res(0,+1) = KS0(0)*KS1(+1);
                }
                if(l0==l1+4) { res(-1,+1) = KS0(-1)*KS1(+1); }
                if(l0==l1-4) { res(+1,-1) = KS0(+1)*KS1(-1); }
                
                return res;
            }
        };

        template<
            Kinetic::Spec kS, 
            Operator::Spec oS, Term::Type tT, Bloch::Type bT
        > requires Kinetic::IK_or_KI<kS>
        struct KronPAB<kS,oS,tT,bT>
        {
            Kin<double> operator()( Calcs<oS,tT,bT> &iC )
            {
                if( iC.sP.mL() != iC.sP.mR() ) { throw; }

                Kin<double> res{ iC.GetSAKin(), { 0. } };

                const auto l0{ iC.sP.l0() };
                const auto l1{ iC.sP.l1() };
                if( (std::max(l0,l1) - std::min(l0,l1)) > 2 ) { return res; }

                std::array<double,3> KSArr { 0. }; Kin<double> KS { KSArr };
                double rE;
                if constexpr(kS == Kinetic::Spec::IK) {
                    KS = iC.sP.KS1();
                    rE = iC.EffectiveRadScaleR();
                }
                else if constexpr(kS == Kinetic::Spec::KI) {
                    KS = iC.sP.KS0();
                    rE = iC.EffectiveRadScaleL();
                }
                if constexpr(WfType == Radial::Type::Slater) { KS(-1) /= rE; }
                if constexpr(WfType == Radial::Type::Gauss) { KS(-1) /= rE*rE; }


                if(l0==l1) { res(0) = KS(0); }
                if constexpr(kS == Kinetic::Spec::IK)
                {
                    if(l0==l1-2) { res(-1) = KS(-1); }
                    if(l0==l1+2) { res(+1) = KS(+1); }
                }
                if constexpr(kS == Kinetic::Spec::KI)
                {
                    if(l0-2==l1) { res(-1) = KS(-1); }
                    if(l0+2==l1) { res(+1) = KS(+1); }
                }
                return res;
            }
        };

        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
        struct KronPAB<Kinetic::Spec::II,oS,tT,bT>
        {
            double operator()( const Calcs<oS,tT,bT> &iC )
            {
                if (iC.sP.mL() != iC.sP.mR()) { throw; }
                if(iC.sP.lL() == iC.sP.lR()) { return 1.; } 
                else { return 0.; }
            }
        };


        template<
            Kinetic::Spec kS, Operator::Spec oS, Term::Type tT, Bloch::Type bT
        >
        struct LegAB{};

        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
        struct LegAB<Kinetic::Spec::KK,oS,tT,bT>{
            KinKin<double> operator()( Calcs<oS,tT,bT>& iC )
            {
                if( !iC.HasEvaluated() )
                {
                    return KronPAB<Kinetic::Spec::KK,oS,tT,bT>()(iC);
                }

                const auto & sP{ iC.sP };

                const auto l0{ sP.l0() };
                const auto l1{ sP.l1() };
                const auto m0{ sP.m0() };
                const auto m1{ sP.m1() };
                const double chi0{ iC.chi(0) };
                const double chi1{ iC.chi(1) };

                std::array<double,3> L0Arr
                {
                    sP.KS0(-1)*sP.NS0(-1)*Legendre(chi0,l0-2,m0),
                    sP.KS0(0)*sP.NS0(0)*Legendre(chi0,l0,m0),
                    sP.KS0(+1)*sP.NS0(+1)*Legendre(chi0,l0+2,m0)
                };
                Kin<double> && L0{ L0Arr };
                std::array<double,3> L1Arr
                {
                    sP.KS1(-1)*sP.NS1(-1)*Legendre(chi1,l1-2,m1),
                    sP.KS1(0)*sP.NS1(0)*Legendre(chi1,l1,m1),
                    sP.KS1(+1)*sP.NS1(+1)*Legendre(chi1,l1+2,m1)
                };
                Kin<double> && L1{ L1Arr };

                double rE0{ iC.EffectiveRadScale(0) };
                if constexpr(WfType == Radial::Type::Slater)
                { L0(-1) /= rE0; }
                if constexpr(WfType == Radial::Type::Gauss)
                { L0(-1) /= rE0*rE0; }
                double rE1{ iC.EffectiveRadScale(1) };
                if constexpr(WfType == Radial::Type::Slater)
                { L1(-1) /= rE1; }
                if constexpr(WfType == Radial::Type::Gauss)
                { L1(-1) /= rE1*rE1; }

                const double N{ 2*iC.ScaledNorm(0)*iC.ScaledNorm(1) };
                return {
                    iC.GetSAKinKin(),
                    {
                        N*L0(-1)*L1(-1),
                        N*L0(0)*L1(-1),
                        N*L0(+1)*L1(-1),
                        N*L0(-1)*L1(0),
                        N*L0(0)*L1(0),
                        N*L0(+1)*L1(0),
                        N*L0(-1)*L1(+1),
                        N*L0(0)*L1(+1),
                        N*L0(+1)*L1(+1),
                    }
                };
            }
        };

        template< 
            Kinetic::Spec kS,
            Operator::Spec oS, Term::Type tT, Bloch::Type bT
        > requires Kinetic::IK_or_KI<kS>
        struct LegAB<kS,oS,tT,bT>{
            Kin<double> operator()( Calcs<oS,tT,bT> &iC )
            {
                if( !iC.HasEvaluated() )
                {
                    return KronPAB<kS,oS,tT,bT>()(iC);
                }

                const auto & sP{ iC.sP };

                const auto l0{ sP.l0() };
                const auto l1{ sP.l1() };
                const auto m0{ sP.m0() };
                const auto m1{ sP.m1() };

                const double chi0{ iC.chi(0) };
                const double chi1{ iC.chi(1) };

                const double N{ 2*iC.ScaledNorm(0)*iC.ScaledNorm(1) };

                std::array<double,3> KmArr { 0. }; Kin<double> Km{ KmArr };
                std::array<double,3> LmArr { 0. }; Kin<double> Lm{ LmArr };
                double rE;
                if constexpr(kS == Kinetic::Spec::IK)
                {
                    double Lx { N*sP.NS0(0)*Legendre(chi0,l0,m0) };
                    Km = sP.KS1();
                    rE = iC.EffectiveRadScale(1);
                    Lm = {
                        (l1 >= 2) ? Lx*sP.NS1(-1)*Legendre(chi1,l1-2,m1) : 0,
                        Lx*sP.NS1(0)*Legendre(chi1,l1,m1),
                        Lx*sP.NS1(+1)*Legendre(chi1,l1+2,m1)
                    };
                }
                else if constexpr(kS == Kinetic::Spec::KI)
                {
                    double Lx { N*sP.NS1(0)*Legendre(chi1,l1,m1) };
                    Km = sP.KS0();
                    rE = iC.EffectiveRadScale(0);
                    Lm = {
                        (l0 >= 2) ? Lx*sP.NS0(-1)*Legendre(chi0,l0-2,m0) : 0,
                        Lx*sP.NS0(0)*Legendre(chi0,l0,m0),
                        Lx*sP.NS0(+1)*Legendre(chi0,l0+2,m0)
                    };
                }


                if constexpr(WfType == Radial::Type::Slater) { Km(-1) /= rE; }
                if constexpr(WfType == Radial::Type::Gauss) { Km(-1) /= rE*rE; }
                

                return {
                    iC.GetSAKin(),
                    {
                        Km(-1)*Lm(-1),
                        Km(0)*Lm(0),
                        Km(+1)*Lm(+1),
                    }
                };
            }
        };

        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
        struct LegAB<Kinetic::Spec::II,oS,tT,bT>{
            double operator()( const Calcs<oS,tT,bT> &iC )
            {
                if( !iC.HasEvaluated() )
                {
                    return KronPAB<Kinetic::Spec::II,oS,tT,bT>()(iC); 
                }

                const auto l0{ iC.sP.l0() };
                const auto l1{ iC.sP.l1() };
                const auto m0{ iC.sP.m0() };
                const auto m1{ iC.sP.m1() };
                const double n0{ iC.sP.NS0(0) };
                const double n1{ iC.sP.NS1(0) };
                const double chi0{ iC.chi(0) };
                const double chi1{ iC.chi(1) };
                const double N{ 2*n0*n1*iC.ScaledNorm(0)*iC.ScaledNorm(1) };
                return N*Legendre(chi0,l0,m0)*Legendre(chi1,l1,m1);
            }
        };

    }
}