namespace Integrand
{
    namespace Radial
    {
        template<
            Operator::Spec oS, Term::Type tT, Bloch::Type bT,
            bool SR,
            size_t iu
        >
        struct Core {

            template< class T >
            static auto _Mesh(T& J)
            {
                if constexpr( Operator::II<oS> ) { return J[0]; }
                else
                {
                    using J_arr_t = std::remove_reference_t<decltype(J)>;
                    using J_t = J_arr_t::value_type;
                    if constexpr( Operator::Kin<oS> )
                    { return Kin<J_t>(J[0],J[1]); }
                    if constexpr( Operator::KK<oS> )
                    { return KinKin<J_t>(J); }
                }
            }

            auto operator()(Calcs<oS,tT,bT>& iC)
            {
                constexpr bool miss{
                    Operator::_Mismatch<tT,bT>
                    ||
                    ::Decay::Wf_Mismatch<oS.Decay(iu)> 
                };
                constexpr bool match{
                    Operator::_Match<tT,bT>
                    ||
                    ::Decay::Wf_Match<oS.Decay(iu)>
                };

                iC.template ZeroJArr<SR,miss,match>();
                constexpr auto Nt{ iC.NumValues() };
                constexpr_for<size_t(0),Nt,size_t(1)>([&](auto g){
                    const auto Nk{ iC.NumKeys(g) };
                    for( ui_t k = 0; k < Nk; k++ )
                    {
                        iC.template AddToJArr<SR,miss,match>(g,k);
                    }
                });
                return _Mesh(iC.template GetArr<SR,miss,match>());
            }
        };
    }
}