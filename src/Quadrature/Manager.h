namespace Integrand
{    
    namespace Quadrature
    {
        // Using Boost Gauss-Kronrod quadrature
        class Manager
        {

            Basis::StatePair* sP{ nullptr };
            Coeff::Element* cE{ nullptr };

        public:

            Manager() = default;
            Manager(Basis::StatePair *sPi, Coeff::Element *cEi) :
                sP{ sPi }, cE{ cEi }
            {}
            void Reinitialize(Basis::StatePair *sPi, Coeff::Element *cEi)
            { 
                sP = sPi; cE = cEi; 
            }

            template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
                requires Candidate<oS,tT,bT>
            void Compute()
            {
                namespace quad = boost::math::quadrature;
                using scheme = quad::gauss_kronrod<double,31>;
                
                if( (*sP.l0()+*sP.l1())%2 == 0 )
                {
                    Calcs<oS,tT,bT> iC(*sP);
                    auto fR = [&](double t){
                        return real(Quadrature::Integrand<oS,tT,bT>(iC,*cE,t));
                    };
                    auto fI = [&](double t){
                        return imag(Quadrature::Integrand<oS,tT,bT>(iC,*cE,t));
                    };
                    IVal[0] = scheme::integrate(fR,0.,1.,75,1e-8,&error[0]);
                    IVal[1] = scheme::integrate(fI,0.,1.,75,1e-8,&error[1]);
                }
                else
                {
                    IVal[0] = 0.;
                    IVal[1] = 0.;
                }


            }
            cmplx value() const
            {
                return std::complex<double>(IVal[0],IVal[1]);
            }

        private:
            typedef boost::array<double,2> state_t;
            state_t IVal;
            state_t error;

        };
    }
}