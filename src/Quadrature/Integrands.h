namespace Integrand
{
    namespace Quadrature
    {
        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
        concept Candidate = ( 
            Operator::Symmetry::Azi<oS> && NaiveNotPerp<tT,bT>
        );        

        template<
            Operator::Spec oS, Term::Type tT,Bloch::Type bT, typename KMesh
        > requires Candidate<oS,tT,bT>
        struct Impl
        {
            cmplx operator()(
                Calcs<oS,tT,bT> &iC, const Coeff::Element &cE,
                const double chi
            ) {
                // Calcs<oS,tT,bT> iC(sP);
                iC.Evaluate(chi);
                cmplx C{ iC.LeadingConstant() * iC.EllipticalScaling() };

                cmplx sum { 0 };
                auto summation { 
                    [&sum,&iC,&cE,&chi](auto ui){
                        using is_t = Summation<oS,tT,bT,true,KMesh,ui>;
                        using is_f = Summation<oS,tT,bT,false,KMesh,ui>;
                        cmplx data{ oS.U0(ui) };
                        if(iC.EffectiveRadialMismatch() == 0)
                        {
                            if(iC.sP.ReverseFlag())
                            { data *= is_t::template f<true>(iC,cE); }
                            else
                            { data *= is_t::template f<false>(iC,cE); }
                        } else {
                            if(iC.sP.ReverseFlag())
                            { data *= is_f::template f<true>(iC,cE); }
                            else
                            { data *= is_f::template f<false>(iC,cE); }
                            
                        }
                        bool LI{ oS.SolidAngle(ui).IsAzimuthallyInvariant() };
                        if( !LI ) { data *= oS.SolidAngle(ui)(chi); }
                        sum += data;
                    }
                };
                constexpr_for<0, oS.nterms(), 1>(summation);

                return C*sum;
            }
        };

        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
            requires Candidate<oS,tT,bT>
        cmplx Integrand(
            Calcs<oS,tT,bT> &iC, const Coeff::Element &cE,
            const double chi
        ) {
            if constexpr( Operator::II<oS> )
            { return Impl<oS,tT,bT,double>()(iC,cE,chi); }
            if constexpr( Operator::Kin<oS> )
            { return Impl<oS,tT,bT,Kin<double>>()(iC,cE,chi); }
            if constexpr( Operator::KK<oS> ) 
            { return Impl<oS,tT,bT,KinKin<double>>()(iC,cE,chi); }
        }
    }
}