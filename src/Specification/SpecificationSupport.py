tab = "    "
nl = "\n"

# def op_tup(PotLabels,pot_spec:dict,indent=""):
#     x = indent+"typedef std::tuple<"
#     x += nl
#     for name in PotLabels[:-1]:
#         v = pot_spec[name]
#         x += indent+tab+"Group<"+str(v[0])+">,"+nl
#     for name in PotLabels[-1:]:
#         v = pot_spec[name]
#         x += indent+tab+"Group<"+str(v[0])+">"+nl
#     x += indent+"> op_tup;"
#     return x

def Parse_lk_Tuple(x):
    return "{{ " + str(x[0]) +"," + str(x[1]) + " }}"
def Parse_Ordered_lks(O,indent):
    N = 5
    return (','+nl).join((
        indent+", ".join( ( Parse_lk_Tuple(x) for x in O[i*N : (i+1)*N] ) )
        for i in range( (len(O)+N-1)//N )
    ))



def ParseLabels(x,indent):
    Line = ""
    Line += (','+nl).join(indent+name for name in x)
    return Line
def ParseSymmetryReprs(x,indent):
    from itertools import chain
    Line = ""
    Line += indent+"using reprs_t = std::array<Repr,"+str(len(x))+">;"+nl
    Line += indent+"static constexpr reprs_t _reprs"+nl
    Line += indent+"{{"+nl
    temp = tuple( indent+tab+y.cpp for y in x )
    Line += (','+nl).join(temp)+nl
    Line += indent+"}};"
    return Line
def ParseRadialExponents(x,indent):
    from itertools import chain
    Line = ""
    Line += indent+"using exps_t = std::array<exp_t,"+str(len(x))+">;"+nl
    Line += indent+"static constexpr exps_t _exps"+nl
    Line += indent+"{{"+nl
    temp = ( map(str,y) for y in x )
    temp = ( indent+tab+"{"+",".join(x)+"}" for x in temp)
    Line += (','+nl).join(temp)+nl
    Line += indent+"}};"
    return Line
def ParsePotsForIndices(d,indent):
    Line = ""
    Line += tab+"using arr_t = std::array<std::array<size_t,4>,NumPotTypes>;"+nl
    Line += tab+"static constexpr arr_t _idx_terms"+nl
    Line += tab+"{{"+nl
    temp = ( ",".join(map(str,term)) for term in d.values() )
    # temp = tuple(
    #     "_specs["+str(x[0])+"],_funcs["+str(x[1])+"],"+str(x[2])+","+str(x[3])
    #     for y in temp for x in y 
    # )
    Line += (','+nl).join( indent+x for x in temp )+nl
    Line += tab+"}};"
    return Line
def ParseFuncArray(g,indent):
    Line = ""
    Line += tab+"using func_a = std::array<Func,"+str(len(g))+">;"+nl
    Line += tab+"static constexpr func_a _funcs"+nl
    Line += tab+"{{"+nl
    temp = tuple(f.cpp_id for f in g)
    Line += (','+nl).join( indent+x for x in temp )+nl
    Line += tab+"}};"
    return Line
def ParseDecaySpecArray(m,indent):
    Line = ""
    Line += tab+"using spec_a = std::array<Spec,"+str(len(m))+">;"+nl
    Line += tab+"static constexpr spec_a _specs"+nl
    Line += tab+"{{"+nl
    temp = tuple( (ds.cpp+",_funcs["+str(n)+"]") for ds,n in m)
    Line += (','+nl).join( indent+"Spec("+x+")" for x in temp )+nl
    Line += tab+"}};"
    return Line
def ParsePotsForExponential(d,indent):
    Line = ""
    Line += tab+"using Terms_t = std::array<Term,NumPotTerms>;"+nl
    Line += tab+"static constexpr Terms_t _exp_terms"+nl
    Line += tab+"{{"+nl
    temp = ( x for y in d.values() for x in y )
    temp = tuple(
        "_specs["+str(x[0])+"],_funcs["+str(x[1])+"],"+str(x[2])+","+str(x[3])
        for x in temp
    )
    Line += (','+nl).join( indent+"Term("+x+")" for x in temp )+nl
    Line += tab+"}};"
    return Line





def ParseSwitch(x,y,indent):
    Line = ""
    Line += (';'+nl).join(indent+"case "+L+" : return "+y[L] for L in x)
    Line += (';'+nl)
    Line += indent+"default : throw;"
    return Line







from OperatorSpecification import CoeffSpecs

def StorageDef(indent):
    Line = ""
    Line += indent+"std::tuple<"+nl
    temp = (','+nl).join( indent+tab+"Pair<"+cS+">" for cS in CoeffSpecs )
    Line += temp+nl
    Line += indent+"> _data;"
    return Line

def InitLoop(indent):
    Line = ""
    N = len(CoeffSpecs)
    Line += nl.join( 
        indent+"std::get<"+str(n)+">(_data).Initialize<dZ>(lki1,lki2);"
        for n in range(N) 
    )
    return Line