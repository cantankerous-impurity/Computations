namespace Potential
{
    void PrintDescription(std::ostream &s)
    {
        s << "PotentialIdx,";
        s << "Exponent,";
        s << "Coefficient(Real),";
        s << "Coefficient(Imag),";
        s << "Symmetry,";
        s << std::endl;
        for(size_t i=1; i<NumHamTerms; i++)
        {
            s << i-1 << ",";
            const auto j{ Potential::_idx_terms[i][1] };
            s << std::get<0>(Potential::_exps[j]) << ",";
            const auto Z{ Potential::_exp_terms[i].U0() };
            s << Z.real() << ",";
            s << Z.imag() << ",";
            // std::cout << std::get<0>(Potential::_exps[i]).Name();
            s << std::endl;
        }

        s << std::endl;

        s << "SolidAngleFunc";
        s << std::endl;
        s << "PotentialIdx,";
        s << "E(Real),E(Imag),";
        s << "X(Real),X(Imag),";
        s << "Y(Real),Y(Imag),";
        s << "Z(Real),Z(Imag),";
        s << "XX(Real),XX(Imag),";
        s << "YY(Real),YY(Imag),";
        s << "ZZ(Real),ZZ(Imag),";
        s << "XY(Real),XY(Imag),";
        s << "XZ(Real),XZ(Imag),";
        s << "YZ(Real),YZ(Imag)";
        s << std::endl;
        for(size_t i=1; i<NumHamTerms; i++)
        {
            s << i-1 << ",";
            const auto F{ Potential::_exp_terms[i].SolidAngle() };
            s << F.Cd(SolidAngle::Dependence::Unity).real() << ",";
            s << F.Cd(SolidAngle::Dependence::Unity).imag() << ",";

            s << F.Cd(SolidAngle::Dependence::FieldX).real() << ",";
            s << F.Cd(SolidAngle::Dependence::FieldX).imag() << ",";

            s << F.Cd(SolidAngle::Dependence::FieldY).real() << ",";
            s << F.Cd(SolidAngle::Dependence::FieldY).imag() << ",";

            s << F.Cd(SolidAngle::Dependence::FieldZ).real() << ",";
            s << F.Cd(SolidAngle::Dependence::FieldZ).imag() << ",";

            s << F.Cd(SolidAngle::Dependence::FieldXX).real() << ",";
            s << F.Cd(SolidAngle::Dependence::FieldXX).imag() << ",";

            s << F.Cd(SolidAngle::Dependence::FieldYY).real() << ",";
            s << F.Cd(SolidAngle::Dependence::FieldYY).imag() << ",";

            s << F.Cd(SolidAngle::Dependence::FieldZZ).real() << ",";
            s << F.Cd(SolidAngle::Dependence::FieldZZ).imag() << ",";

            s << F.Cd(SolidAngle::Dependence::FieldXY).real() << ",";
            s << F.Cd(SolidAngle::Dependence::FieldXY).imag() << ",";

            s << F.Cd(SolidAngle::Dependence::FieldXZ).real() << ",";
            s << F.Cd(SolidAngle::Dependence::FieldXZ).imag() << ",";

            s << F.Cd(SolidAngle::Dependence::FieldYZ).real() << ",";
            s << F.Cd(SolidAngle::Dependence::FieldYZ).imag();
            s << std::endl;
        }

        s << std::endl;

        s << "SlaterFunc";
        s << std::endl;
        s << "PotentialIdx,";
        s << "E(Real),E(Imag),";
        s << "X(Real),X(Imag),";
        s << "Y(Real),Y(Imag),";
        s << "Z(Real),Z(Imag),";
        s << "XX(Real),XX(Imag),";
        s << "YY(Real),YY(Imag),";
        s << "ZZ(Real),ZZ(Imag),";
        s << "XY(Real),XY(Imag),";
        s << "XZ(Real),XZ(Imag),";
        s << "YZ(Real),YZ(Imag)";
        s << std::endl;
        for(size_t i=1; i<NumHamTerms; i++)
        {
            const auto F{ Potential::_exp_terms[i].Decay().YukawaDecay() };
            s << i-1 << ",";
            s << -F.Cd(SolidAngle::Dependence::Unity).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::Unity).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldX).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldX).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldY).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldY).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldZ).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldZ).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldXX).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldXX).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldYY).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldYY).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldZZ).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldZZ).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldXY).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldXY).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldXZ).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldXZ).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldYZ).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldYZ).imag();
            s << std::endl;
        }

        s << std::endl;

        s << "GaussFunc";
        s << std::endl;
        s << "PotentialIdx,";
        s << "E(Real),E(Imag),";
        s << "X(Real),X(Imag),";
        s << "Y(Real),Y(Imag),";
        s << "Z(Real),Z(Imag),";
        s << "XX(Real),XX(Imag),";
        s << "YY(Real),YY(Imag),";
        s << "ZZ(Real),ZZ(Imag),";
        s << "XY(Real),XY(Imag),";
        s << "XZ(Real),XZ(Imag),";
        s << "YZ(Real),YZ(Imag)";
        s << std::endl;
        for(size_t i=1; i<NumHamTerms; i++)
        {
            const auto F{ Potential::_exp_terms[i].Decay().GaussianDecay() };
            s << i-1 << ",";
            s << -F.Cd(SolidAngle::Dependence::Unity).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::Unity).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldX).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldX).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldY).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldY).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldZ).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldZ).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldXX).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldXX).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldYY).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldYY).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldZZ).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldZZ).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldXY).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldXY).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldXZ).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldXZ).imag() << ",";

            s << -F.Cd(SolidAngle::Dependence::FieldYZ).real() << ",";
            s << -F.Cd(SolidAngle::Dependence::FieldYZ).imag();
            s << std::endl;
        }
    }    
}

namespace Operator
{
    // [SystemDielectric set in Operators/Spec.h]
    constexpr double epSili0{ SystemDielectric.StaticDielectric() }; // unitless
    constexpr double epSili{ epSili0*Ep }; // C^2 / eV m

    void PrintMassAniso(std::ostream &s)
    {
        s << "MassAniso: " << mass_aniso;
        s << std::endl;
    }
    void PrintRelativeDielectric(std::ostream &s)
    {
        s << "RelativeDielectric: " << Operator::epSili0;
        s << std::endl;
    }
}

namespace SystemUnits
{
    // [System units defined from dielectric]
    // length [in meters]
    constexpr double Length{
        ( 4*PI*Operator::epSili*HBar*HBar ) / ( mT*ChargeSI*ChargeSI ) 
    };
    // energy [in electronvolts]
    constexpr double Energy{
        []{
            constexpr double _n{ mT*ChargeSI*ChargeSI*ChargeSI*ChargeSI };
            constexpr double _x{ 4*PI*Operator::epSili*HBar };
            constexpr double _d{ 2*_x*_x };
            return _n/_d;
        }()
    };
    // wave vectors in system units
    constexpr double  k_Si =  ::k_Si * Length;
    constexpr double k_val = ::k_val * Length;

    void PrintEnergy(std::ostream &s)
    {
        s << "Energy SU(meV): " << Energy*1e3;
        s << std::endl;
    }
    void PrintLength(std::ostream &s)
    {
        s << "Length SU(nm): " << Length*1e9;
        s << std::endl;
    }

}