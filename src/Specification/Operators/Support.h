namespace Bloch
{
    struct Type
    {
        enum Enum { Naive=0, Trunc=1 };
        static constexpr int _NumTypes { 2 };
        static constexpr int NumTypes() { return _NumTypes; }

        const Enum _value;
        constexpr Enum value() const { return _value; }

        constexpr Type() = delete;
        constexpr Type(const Enum in) : _value{ in } {}
        constexpr Type(int in) : Type(static_cast<Enum>(in)) {}
        constexpr Type(const Type& in) : _value{ in.value() } {}

        constexpr operator Enum() const { return _value; }

        constexpr bool IsNaive() const { return _value == Naive; }
        constexpr bool IsTrunc() const { return _value == Trunc; }   
    };

    template< Type bT >
    concept IsNaive = requires { requires bT == Type::Naive; };
    template< Type bT >
    concept IsTrunc = !IsNaive<bT>;
}

namespace Term
{
    struct Type
    {
        enum Enum { Parallel=0, Antiparallel=1, Perpendicular=2 };
        static constexpr int _NumTypes { 3 };
        static constexpr int NumTypes() { return _NumTypes; }

        const Enum _value;
        constexpr Enum value() const { return _value; }

        constexpr Type() = delete;
        constexpr Type(const Enum in) : _value{ in } {}
        constexpr Type(int in) : Type(static_cast<Enum>(in)) {}
        constexpr Type(const Type& in) : _value{ in.value() } {}

        constexpr operator Enum() const { return _value; }

        constexpr bool IsParallel() const { return _value == Parallel; }
        constexpr bool IsAntiparallel() const { return _value == Antiparallel; }
        constexpr bool IsPerp() const { return _value == Perpendicular; }
        constexpr bool IsNotPerp() const { return _value != Perpendicular; }

        const std::string name() const
        {
            switch(_value)
            {
                case Parallel : return "Parallel";
                case Antiparallel : return "Antiparallel";
                case Perpendicular : return "Perpendicular";
            }
        }

        constexpr std::array<AnisoDirection,2> Dirs() const
        {
            using enum AnisoDirection::Enum;
            switch(_value)
            {
                case Parallel :
                case Antiparallel :
                    return {Z,Z};
                case Perpendicular :
                    return {Y,X};
            }
        }

        const std::string DirSpec() const
        {
            switch(_value)
            {
                case Parallel : return "bZZ";
                case Antiparallel : return "ZZ";
                case Perpendicular : return "YX";
                default : throw;
            }
        }

        constexpr bool SameDir() const
        {
            switch(_value)
            {
                case Parallel :
                case Antiparallel :
                    return true;
                case Perpendicular :
                    return false;
                default : 
                    return false;
            }
        }
    };

    template< Type tT >
    concept Para = requires { requires tT == Type::Parallel; };
    template< Type tT >
    concept Antipara = requires { requires tT == Type::Antiparallel; };
    template< Type tT >
    concept Perp = requires { requires tT == Type::Perpendicular; };
    template< Type tT >
    concept NotPerp = !Perp<tT>;

}

template< Term::Type tT, Bloch::Type bT >
concept NaivePara = Term::Para<tT> && Bloch::IsNaive<bT>;
template< Term::Type tT, Bloch::Type bT >
concept NaiveAntipara = Term::Antipara<tT> && Bloch::IsNaive<bT>;
template< Term::Type tT, Bloch::Type bT >
concept NaiveNotPerp = Term::NotPerp<tT> && Bloch::IsNaive<bT>;
template< Term::Type tT, Bloch::Type bT >
concept NaiveAndPerp = Term::Perp<tT> && Bloch::IsNaive<bT>;



namespace Potential
{

    using Dep = SolidAngle::Dependence;
    using Func = SolidAngle::Func;
    using Spec = Decay::Spec;
    using Repr = Td::Representation;
    
    using enum Dep::Enum;
    using enum Repr::Enum;

    using exp_t = std::tuple< double,const char* >;

    struct Term
    {
        const Spec _s;
        const Func _d;
        const cmplx _Uc;
        const double _p;

        Term() = delete;
        constexpr Term(
            const Spec s,
            const Func d,
            const cmplx Uc,
            const double p
        )
        :
            _s{s}, _d{d}, _Uc{Uc}, _p{p}
        {}
        constexpr Term(
            const Spec s,
            const Dep::Enum d,
            const cmplx Uc,
            const double p
        )
        :
            _s{s}, _d{Func(1,Dep(d))}, _Uc{Uc}, _p{p}
        {}

        constexpr auto& Decay() const { return _s; }
        constexpr auto& SolidAngle() const { return _d; }
        constexpr auto& U0() const { return _Uc; }
        constexpr auto phase() const { return _p; }
    };
}

constexpr int TermIndex(Term::Type tT, Bloch::Type bT)
{
    return int(tT) + tT.NumTypes()*int(bT);
}
constexpr Term::Type GetTermType(int ti)
{
    return static_cast<Term::Type::Enum>(ti/Term::Type::NumTypes());
}
constexpr Bloch::Type GetBlochType(int ti) {
    return static_cast<Bloch::Type::Enum>(ti%Term::Type::NumTypes()); 
}

namespace Operator
{
    namespace Symmetry
    {
        template< Term::Type tT >
        constexpr cmplx Prefactor(
            const Td::Representation r,
            const Td::Decomposition v0, const Flip s0,
            const Td::Decomposition v1, const Flip s1
        ) {
            using enum Term::Type::Enum;
            if constexpr(tT == Perpendicular)
            { return PerpPref(r,v0,s0,v1,s1); }
            else
            { return ParaPref(r,v0,s0,v1,s1); }
        }
    }
}