namespace Decay
{
    struct Type
    {

        enum Enum { Zero,Yukawa,Gauss,YukGau };
        const Enum _value;

        Type() = delete;
        constexpr Type(Enum in) : _value(in) {}

        // Allow switch and comparisons.
        constexpr operator Enum() const { return _value; }
        // Prevent usage: if(OperatorSpec)
        explicit operator bool() = delete;

        constexpr bool HasNoDecay() const
        {
            switch (_value)
            {
                case Zero:
                    return true;
                default:
                    return false;
            }
        }
        constexpr bool HasYukawaDecay() const
        {
            switch (_value)
            {
                case Yukawa:
                case YukGau:
                    return true;
                default:
                    return false;
            }
        }
        constexpr bool HasGaussianDecay() const
        {
            switch (_value)
            {
                case Gauss:
                case YukGau:
                    return true;
                default:
                    return false;
            }
        }
        constexpr bool HasCombinationDecay() const
        {
            switch (_value)
            {
                case YukGau:
                    return true;
                default:
                    return false;
            }
        }
        constexpr bool HasXorDecay() const
        {
            switch (_value)
            {
                case Yukawa:
                case Gauss:
                    return true;
                default:
                    return false;
            }
        }

        constexpr bool UsesNoZ() const
        {
            if( HasNoDecay() ) { return true; }
            return false;
        }
        constexpr bool UsesZAlpha() const
        {
            if( HasYukawaDecay() && !HasGaussianDecay() ) { return true; }
            return false;
        }
        constexpr bool UsesZBeta() const
        {
            if( !HasYukawaDecay() && HasGaussianDecay() ) { return true; }
            return false;
        }
        constexpr bool UsesZAlphaBeta() const
        {
            if( HasCombinationDecay() ) { return true; }
            return false;
        }

    };

    

    struct Spec
    {
        using Func = SolidAngle::Func;
        using enum Decay::Type::Enum;

        static constexpr const Func EmptyFunc{ Func(0.) };

        static constexpr Type DetermineType( Func a, Func b )
        {
            if ( !a.IsEmpty() && !b.IsEmpty() ) { return YukGau; }
            if ( !a.IsEmpty() &&  b.IsEmpty() ) { return Yukawa; }
            if (  a.IsEmpty() && !b.IsEmpty() ) { return Gauss; }
            if (  a.IsEmpty() &&  b.IsEmpty() ) { return Zero; }
            return Zero;
        }

        const Type _t;
        const Func &_da;
        const Func &_db;

        constexpr Spec( const Func &da, const Func &db )
        :
            _t{ DetermineType(da,db) },
            _da{ da },
            _db{ db }
        {}
        constexpr Spec( const Type t, const Func &&dc )
        :
            _t{ t },
            _da{ t == Yukawa ? dc : EmptyFunc },
            _db{ t == Gauss ? dc : EmptyFunc }
        {}
        constexpr Spec( const Type t, const Func &dc )
        :
            _t{ t },
            _da{ t == Yukawa ? dc : EmptyFunc },
            _db{ t == Gauss ? dc : EmptyFunc }
        {}
        constexpr Spec() : Spec( EmptyFunc, EmptyFunc ) {}

        // Allow switch and comparisons.
        constexpr operator Type() const { return _t; }
        // Prevent usage: if(Spec)
        explicit operator bool() = delete;


        constexpr bool HasNoDecay() const { return _t.HasNoDecay(); }
        constexpr bool HasYukawaDecay() const
        {
            return _t.HasYukawaDecay();
        }
        constexpr bool HasGaussianDecay() const
        {
            return _t.HasGaussianDecay();
        }


        constexpr Flip AzimuthalParity() const
        {
            const bool daPar {
                _da.IsEmpty() || ( _da.AzimuthalParity() == Flip::Heads )
            };
            bool dbPar {
                _db.IsEmpty() || ( _db.AzimuthalParity() == Flip::Heads )
            };
            if ( daPar && dbPar ) { return Flip::Heads; }
            return Flip::undef;
        }
        constexpr Flip LongitudinalParity() const
        {
            const bool daPar {
                _da.IsEmpty() || ( _da.LongitudinalParity() == Flip::Heads )
            };
            bool dbPar {
                _db.IsEmpty() || ( _db.LongitudinalParity() == Flip::Heads )
            };
            if ( daPar && dbPar ) { return Flip::Heads; }
            return Flip::undef;
        }


        constexpr bool IsAzimuthallyInvariant() const
        {
            return (
                _da.IsAzimuthallyInvariant()
                &&
                _db.IsAzimuthallyInvariant()
            );
        }
        constexpr bool IsSphericallyInvariant() const
        {
            return (
                _da.IsSphericallyInvariant()
                &&
                _db.IsSphericallyInvariant()
            );
        }


        constexpr const Func& YukawaDecay() const
        {
            switch (_t)
            {
                case Yukawa:
                case YukGau:
                    return _da;
                default:
                    return EmptyFunc;
            }
        }
        constexpr const Func& GaussianDecay() const
        {
            switch (_t)
            {
                case Gauss:
                case YukGau:
                    return _db;
                default:
                    return EmptyFunc;
            }
        }
    };

    template< Type D >
    concept Zero = requires { requires D.HasNoDecay(); };

    template< Type D >
    concept NonZero = !Zero<D>;

    template< Type D >
    concept Combination = requires { requires D.HasCombinationDecay(); };

    template< Type D >
    concept Xor = requires { requires D.HasXorDecay(); };

    template< Type D >
    concept UsesNoZ = requires { requires D.UsesNoZ(); };

    template< Type D >
    concept UsesZAlpha = requires { requires D.UsesZAlpha(); };

    template< Type D >
    concept UsesZBeta = requires { requires D.UsesZBeta(); };

    template< Type D >
    concept UsesZAlphaBeta = requires { requires D.UsesZAlphaBeta(); };

    
    
    template< Type D >
    concept Yukawa = requires { requires D.HasYukawaDecay(); };

    template< Type D >
    concept Gauss = requires { requires D.HasGaussianDecay(); };
}