from os import environ

from SpecificationClasses import RadialType, Td_Decomposition

from BasisSupport import SubBasis, SuperBasis
from CoefficientSupport import CoeffsDB

id="BasisA"

Loc = environ.get("basisPath")


rT = RadialType.Slater
TotalBasis = SuperBasis(
    (
        SubBasis(
            (
                (0,0,0),(0,1,0)
            ),
            Td_Decomposition.E1,0.62,1.54
        ),

        # SubBasis(
        #     (
        #         (0,0,0),#(0,0,1),(0,0,2),(0,0,3),(0,0,4),
        #         # (0,2,0),(0,2,1),(0,2,2),(0,2,3),(0,2,4),
        #         # (0,4,0),(0,4,1),(0,4,2),(0,4,3),(0,4,4),
        #         # (0,6,0),(0,6,1),(0,6,2),(0,6,3),(0,6,4),
        #         # (0,8,0),(0,8,1),(0,8,2),(0,8,3),(0,8,4),
        #     ),
        #     Td_Decomposition.E1,0.62,1.54
        # ),

        # SubBasis(
        #     (
        #         (0,1,0),#(0,1,1),(0,1,2),(0,1,3),(0,1,4),
        # #         (0,3,0),(0,3,1),(0,3,2),(0,3,3),
        # #         (0,5,0),(0,5,1),(0,5,2),(0,5,3),
        # #         (0,7,0),(0,7,1),(0,7,2),(0,7,3),
        # #         (0,9,0),(0,9,1),(0,9,2),(0,9,3),
        #     ),
        #     Td_Decomposition.E1,0.5,1.5
        # ),

        # SubBasis(
        #     (
        #         (0,0,0),(0,0,1),(0,0,2),(0,0,3),
        #         (0,2,0),(0,2,1),(0,2,2),(0,2,3),
        #         (0,4,0),(0,4,1),(0,4,2),(0,4,3),
        #         (0,6,0),(0,6,1),(0,6,2),(0,6,3),
        #     ),
        #     Td_Decomposition.E1,1.24,1.72
        # ),

        # SubBasis(
        #     (
        #         (0,0,0),(0,0,1),(0,0,2),
        #         (0,2,0),(0,2,1),(0,2,2),
        #         (0,4,0),(0,4,1),(0,4,2),
        #     ),
        #     Td_Decomposition.E1,1.8,2.0
        # ),

    ),
    CoeffsDB
)


NumBases = TotalBasis.NumBases
NumParamPairs = (NumBases*(NumBases+1))//2
NumStates = TotalBasis.NumStates
NumElems = (NumStates*(NumStates+1))//2
NumLKs = TotalBasis.NumLKs
NumLKPairs = (NumLKs*(NumLKs+1))//2