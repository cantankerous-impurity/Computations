namespace Basis
{
    void Print(std::ostream &s)
    {
        PrintWfType(s);
        s << std::endl;
        s << "BasisIdx,";
        s << "m,";
        s << "l,";
        s << "k,";
        s << "Azimuth,";
        s << "Symmetry,";
        s << std::endl;
        constexpr_for<0,NumStates,1>(
            [&](auto i){
                const auto statePtr{ Basis::CurrBases.GetState<i>() };
                s << i << ",";
                s << (int)statePtr->m() << ",";
                s << (int)statePtr->l() << ",";
                s << (int)statePtr->k() << ",";
                s << (int)statePtr->Azimuth() << ",";
                s << statePtr->Decomp().Symmetry().Name();
                s << std::endl;
            }
        );
    }
}