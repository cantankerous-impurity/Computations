from os import environ

from math import factorial

from itertools import combinations
from itertools import combinations_with_replacement as cwr

from SpecificationClasses import DielectricType
from SpecificationClasses import DecaySpec,DecayType
from SpecificationClasses import SolidAngleFunc
from SpecificationClasses import Td_Representation

from OperatorSupport import PotSquare,PotProduct,get_OpId
# from OperatorSupport import parse_PotExp
from OperatorSupport import parsekin_Pot,parsekin_PP,parsekin_PaPb
from OperatorSupport import parsepot_Pot,parsepot_PP, parsepot_PaPb

Loc = environ.get("opPath")
Name = "OperatorSpec"
id = "OperatorA"

# Anisotropy of kinetic energy induced due to anisotropic effective mass
# mass_aniso = 1
mass_aniso = 0.1905/0.9163

# SingleValleyModel = False
# NaiveBloch = False

# SingleValleyModel = False
# NaiveBloch = True

SingleValleyModel = True
NaiveBloch = True


zero = DecaySpec()
unity = SolidAngleFunc(1)


SystemDielectric = DielectricType.Static
static_group = ( ( zero , unity, -2., ), )


NT = 0
UC = 500
alX = (NT*UC)/2
al = DecaySpec(DecayType.Yukawa,alX)

def CapF(n):
    return (2/NT)*(NT-n)*pow(alX,n)/factorial(n)

PotSpec = tuple(
    (
        ( "Coulomb", "Cou", -1, Td_Representation.A1, static_group ),
    )
    +
    tuple(
        (
            "Cap"+str(n), "C"+str(n), n-1, Td_Representation.A1,
            ( ( al , unity, CapF(n), ), )
        )
        for n in range(NT)
    )
)


NPots = len(PotSpec)

from CoefficientSupport import MakeGConf
GConf = MakeGConf( (exp for (_,_,exp,_,_) in PotSpec) )


HamLabels = ( "Kinetic", ) + tuple( x[0] for x in PotSpec )
NHamTerms = len(HamLabels)

HamHamLabels = (
    ( "KinKin", )
    +
    tuple(
        lab
        for labs in ( ("Kin"+abbr,abbr+"Kin") for (_,abbr,_,_,_) in PotSpec )
        for lab in labs
    )
    +
    tuple( x[1]+y[1] for x,y in cwr(PotSpec,2) )
)
NHHTerms = len(HamHamLabels)

OpLabels = ( "Overlap", ) + HamLabels + HamHamLabels
NOps = len(OpLabels)






PotSpec = tuple(
    ( 
        name,abbr,exp,repr,
        tuple(
            term 
            if len(term) == 4 else 
            term + (0,)
            for term in grp
        )
    )
    for name,abbr,exp,repr,grp in PotSpec
)

PotDict = (
    { "Identity" : ( 0, Td_Representation.A1, ( (zero,unity,1,0,), ) ) }
    |
    { p[0] : p[2:] for p in PotSpec }
    |
    dict(PotSquare(p) for p in PotSpec)
    |
    dict(PotProduct(pa,pb) for pa,pb in combinations(PotSpec,2))
)
NPotTerms = sum(len(v[2]) for v in PotDict.values())
NPotTypes = len(PotDict)


def RefinePotDict(d:dict):

    _exps = {}
    for k,v in d.items():
        try:
            _exps[v[0]] += (k,)
        except:
            _exps[v[0]] = (k,)
    Exps = tuple( ( ex, get_OpId(ex) ) for ex in _exps.keys() )
    PotExps = { k : n for n,v in enumerate(_exps.values()) for k in v }

    _reprs = {}
    for k,v in d.items():
        try:
            _reprs[v[1]] += (k,)
        except:
            _reprs[v[1]] = (k,)
    Reprs = tuple( x for x in _reprs.keys() )
    PotReprs = { k : n for n,v in enumerate(_reprs.values()) for k in v }

    PotTermGroups = { k : v[2] for k,v in d.items() }

    i1 = -1
    TermIdxs = { 
        k : ( i0, i1, )
        for k,v in PotTermGroups.items()
        if ( (i0:=i1+1) is not None and (i1:=i0+len(v)-1) is not None )
    }

    SpecGroup = tuple({ w[0] for v in PotTermGroups.values() for w in v })
    # DecTypeSet = { x.dec_type for x in SpecGroup }
    FuncSetA = { x.func for x in SpecGroup }
    FuncSetB = { w[1] for v in PotTermGroups.values() for w in v }
    FuncGroup = tuple( FuncSetA | FuncSetB )

    SpecFuncs = tuple(
        (x.dec_type, FuncGroup.index(x.func))
        for x in SpecGroup
    )
    TermMask  = {
        k : tuple(
            ( SpecGroup.index(w[0]), FuncGroup.index(w[1]), ) + w[2:]
        for w in v )
        for k,v in PotTermGroups.items()
    }

    PotDict = {
        k : (PotReprs[k],PotExps[k],) + TermIdxs[k]
        for n,k in enumerate(d.keys()) 
    }

    res = PotDict,TermMask,SpecFuncs,FuncGroup,Exps,Reprs,

    return res


PotDict,TermMask,SpecFuncs,FuncGroup,Exps,Reprs, = RefinePotDict(PotDict)
PotLabels = tuple(PotDict.keys())



Kins = (
    {
        "Overlap":"Kinetic::Spec::II",
        "Kinetic":"Kinetic::Spec::IK",
        "KinKin":"Kinetic::Spec::KK",
    }
    |
    { k:v for x in PotSpec for k,v in parsekin_Pot(x) }
    |
    { k:v for x,y in cwr(PotSpec,2) for k,v in parsekin_PaPb(x,y) }
)
KinsX = { oL : Kins[oL][:-2]+Kins[oL][-1]+Kins[oL][-2] for oL in OpLabels }

Pots = (
    {
        "Overlap" : "Potential::Identity",
        "Kinetic" : "Potential::Identity",
        "KinKin" : "Potential::Identity",
    }
    |
    { k:"Potential::"+x[0] for x in PotSpec for k in parsepot_Pot(x) }
    |
    { k:"Potential::"+k for x,y in cwr(PotSpec,2) for k in parsepot_PaPb(x,y) }
)



def GetId(k):
    return str(PotDict[k][1])
Ids = (
    {
        "Overlap" : GetId("Identity"),
        "Kinetic" : GetId("Identity"),
        "KinKin" : GetId("Identity"),
    }
    |
    { k:GetId(x[0]) for x in PotSpec for k in parsepot_Pot(x) }
    |
    { k:GetId(k) for x,y in cwr(PotSpec,2) for k in parsepot_PaPb(x,y) }
)

def GetType(k):
    temp0 = { SpecFuncs[x[0]][0] for x in TermMask[k] }
    temp1 = { SpecFuncs[x[0]][0]+DecayType.Yukawa for x in TermMask[k] }
    return tuple( x.cpp for x in (temp0 | temp1) )
Types = (
    {
        "Overlap" : GetType("Identity"),
        "Kinetic" : GetType("Identity"),
        "KinKin" : GetType("Identity"),
    }
    |
    { k:GetType(x[0]) for x in PotSpec for k in parsepot_Pot(x) }
    |
    { k:GetType(k) for x,y in cwr(PotSpec,2) for k in parsepot_PaPb(x,y) }
)



# CoeffSpecs = (
#     tuple({
#         "Kinetic::Spec::II,0,Decay::Type::Zero",
#         "Kinetic::Spec::II,0,Decay::Type::Yukawa",
#         "Kinetic::Spec::IK,0,Decay::Type::Zero",
#         "Kinetic::Spec::IK,0,Decay::Type::Yukawa",
#         "Kinetic::Spec::KI,0,Decay::Type::Zero",
#         "Kinetic::Spec::KI,0,Decay::Type::Yukawa",
#     }
#     |
#     {
#         ",".join( (Kins[oL],str(Ids[oL]),Types[oL].cpp,) ) 
#         for oL in OpLabels[2:]
#     })
# )
CoeffSpecs = tuple(sorted(
    {
        ",".join( (KinsX[oL],Ids[oL],x,) ) 
        for oL in OpLabels
        for x in Types[oL]
    }
    |
    {
        ",".join( (Kins[oL],Ids[oL],x,) ) 
        for oL in OpLabels
        for x in Types[oL]
    }
))