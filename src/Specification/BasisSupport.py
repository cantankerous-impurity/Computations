from SpecificationSupport import nl,tab

from SpecificationClasses import Td_Decomposition
from SpecificationClasses import Flip
from SpecificationClasses import keyPart_tuple, basisSpec_tuple
from SpecificationClasses import Ordered_lks, Distinct_lks

from DatabaseInterface import DBInterface


class SuperBasis:

    @staticmethod
    def Insert_lks(kS:keyPart_tuple,DB:DBInterface):
        from CoefficientInterface import g,hi,j
        from OperatorSpecification import GConf
        for f in (g,hi,j,):
            Tbl = f(DB)
            for Conf in GConf:
                Tbl.Insert(kS,Conf)

    def __init__(self,B:tuple['SubBasis',...],DB:DBInterface=None):
        dB = {}
        for b in B:
            p,ri = b.Params,b.Raw_Indices
            if p not in dB: dB[p] = ri
            else:           dB[p] = tuple(set(dB[p] + ri))
        self.Bases = tuple( SubBasis(ri,*p) for p,ri in dB.items() )
        self.BasisSize = tuple(len(b) for b in self.Bases)
        self.NumStates = sum(self.BasisSize)

        if DB is None: self.OrderedSet = False
        else:          self.SetOrdered_lks(DB)

    def Read(self):
        res = ( "{", )
        for b in self.Bases:
            ixs = b.Refined_Indices if self.OrderedSet else b.Indices
            x = list( tab+l for l in self.ReadBasis(b,ixs) )
            x[-1] += ","
            res += tuple(x)
        res += ( "}", )
        return res

    @staticmethod
    def ReadBasis(b:'SubBasis',ixs):
        N = str(len(ixs))
        return (
            ( "SubBasis<"+N+">{", )
            + ( tab+"{ "+b.ReadParams+" },", )
            + ( tab+"IndexArray<"+N+">{{", )
            + tuple( 
                2*tab+"{ "
                    + ','.join( (str(x[0]),str(x[1]),x[2].cpp,x[3].cpp) )
                    + " },"
                for x in ixs 
            )
            + ( tab+"}}", )
            + ( "}", )
        )

    def SetOrdered_lks(self,DB:DBInterface):
        ri = tuple(sorted({x for b in self.Bases for x in b.Raw_Indices}))
        
        self.Insert_lks(Distinct_lks(ri),DB)
        
        self.O = Ordered_lks(ri,DB)
        self.NumLKs = self.O.NumLKs

        for b in self.Bases:
            b.Refined_Indices = tuple(
                ( m,lki,zeta,Omega, )
                for (m,l,k,zeta,Omega) in b.Indices
                if ( lki := self.O.index((l,k,)) ) is not None
            )
        
        self.OrderedSet = True

    @property
    def NumBases(self):
        return len(self.Bases)

    def __len__(self):
        return self.NumStates

    def __add__(self,other):
        if isinstance(other, SubBasis):
            sB = self.Bases
            for n,b in enumerate(self.Bases):
                if b.Params == other.Params:
                    sB[n] = b+other
            return SuperBasis(sB)
        if isinstance(other, SuperBasis):
            return SuperBasis ( self.Bases + other.Bases )
            
    def __radd__(self,other): 
        if other == 0:
            return self
        else:
            return self.__add__(other)


class SubBasis:

    def __init__(self,
        raw_indices:basisSpec_tuple,
        d:Td_Decomposition,
        r:float=1.0,
        a:float=1.0
    ):
        raw_indices = sorted(set(raw_indices))

        self.Decomp = d
        self.Aniso = a
        self.Radius = r

        sigma = d.Acyclic.value
        omega = d.Inversion.value

        self.Indices = (
            tuple( 
                ( m,l,k,zeta,Omega, )
                for (m,l,k) in raw_indices
                if m==0
                if (
                    zeta  := Flip.Heads,
                    Omega := Flip(omega*(-1)**(l)),
                ) is not None
            )
            +
            tuple(
                ( m,l,k,zeta,Omega, )
                for (m,l,k) in raw_indices
                if ( m != 0 ) and ( m//2 == 0 )
                if sigma != 0
                if (
                    zeta  := Flip(sigma*(-1)**(m/2)),
                    Omega := Flip(omega*(zeta.value)*(-1)**(l+m)),
                ) is not None
            )
            +
            tuple(
                ( m,l,k,zeta,Omega, )
                for (m,l,k) in raw_indices
                if ( m != 0 ) and ( m//2 == 0 )
                if sigma == 0
                for zeta in Flip 
                if (
                    Omega := Flip(omega*(zeta.value)*(-1)**(l+m)),
                ) is not None
            )
            +
            tuple(
                ( m,l,k,zeta,Omega, )
                for (m,l,k) in raw_indices
                if m//2 == 1 and omega == -1
                for zeta in Flip
                if (
                    Omega := Flip(omega*(zeta.value)*(-1)**(l+m)),
                ) is not None
            )
        )
        self.Raw_Indices = tuple(sorted({x[:3] for x in self.Indices}))
        self.Refined_Indices = ()

    def __len__(self): return len(self.Indices)

    def __add__(self,other:'SubBasis'):
        if isinstance(other,SubBasis):
            if self.Params != other.Params:
                return SuperBasis( (self,other,) )
            else:
                ri = sorted(set(self.Raw_Indices))
                return SubBasis(ri,self.Decomp,self.Radius,self.Aniso)
        if isinstance(other,basisSpec_tuple):
            ri = sorted(set(self.Raw_Indices+other))
            return SubBasis(ri,self.Decomp,self.Radius,self.Aniso)

    def __radd__(self,other): 
        if other == 0:
            return self
        else:
            return self.__add__(other)

    @property
    def Params(self):
        return (self.Decomp,self.Aniso,self.Radius,)

    @property
    def ReadParams(self):
        return ','.join( (str(self.Decomp),str(self.Aniso),str(self.Radius),) )



    # @staticmethod
    # def ValComb(d:Td_Decomposition,x:Flip):
    #     if d == Td_Decomposition.A1 or d == Td_Decomposition.A2:
    #         return Valley_Combination.A
    #     if d == Td_Decomposition.T2_x or Td_Decomposition.T1_x:
    #         return Valley_Combination.Tx
    #     if d == Td_Decomposition.T2_y or Td_Decomposition.T1_y:
    #         return Valley_Combination.Ty
    #     if d == Td_Decomposition.T2_z or Td_Decomposition.T1_z:
    #         return Valley_Combination.Tz
    #     if d == Td_Decomposition.E1 and x == Flip.Heads:
    #         return Valley_Combination.E1_Even
    #     if d == Td_Decomposition.E2 and x == Flip.Heads:
    #         return Valley_Combination.E2_Even
    #     if d == Td_Decomposition.E1 and x == Flip.Tails:
    #         return Valley_Combination.E1_Odd
    #     if d == Td_Decomposition.E2 and x == Flip.Tails:
    #         return Valley_Combination.E2_Odd



def GetRadii(x,W):
    return (x[0]/(1+x[1])**(1./W),x[0]/(1-x[1])**(1./W))
rads = GetRadii( (.9030, .5848), 1 )