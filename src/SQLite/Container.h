namespace SQL
{
    template < typename d_t >
    class ContainerBase
    {
    public:

        using dvec_t = std::vector<d_t>;

    protected:

        dvec_t _vec;

    public:

        ContainerBase() = default;

        void reserve(size_t NC) { _vec.reserve(NC); }
        void push_back (d_t x) { _vec.push_back(x); }
        d_t& data(size_t n)
        {
            if ( n < size() ) {
                return _vec[n];
            } else {
                throw std::out_of_range(
                    "SQLite container: index out of range."
                );
            }
        }

        size_t size() const { return _vec.size(); }
        d_t data(size_t n) const
        {
            if ( n < size() ) {
                return _vec[n];
            } else {
                throw std::out_of_range(
                    "SQLite container: index out of range."
                );
            }
        }
    };

    template < unsigned int NI, typename i_t, typename v_t >
    struct ContainerWithIndex : public ContainerBase<std::tuple<i_t,v_t>>
    {
        using Base = ContainerBase<std::tuple<i_t,v_t>>;
        using Base::data;
        using Base::push_back;

        v_t& value(size_t n) { return std::get<v_t>(data(n)); }
        i_t& index(size_t n) { return std::get<i_t>(data(n)); }
        v_t value(size_t n) const { return std::get<v_t>(data(n)); }
        i_t index(size_t n) const { return std::get<i_t>(data(n)); }
        int index(size_t n, unsigned int i) const
        {
            if ( i < NI ) {
                return index(n)[i];
            } else {
                throw std::out_of_range( "index: out of range." );
            }
        }
    };
}