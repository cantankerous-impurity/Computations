namespace Bloch
{

    struct Spec
    {
        using vec_t = std::array<double,3>;

        const size_t ci;
        const vec_t gvec;
        const Flip _anti;
        const Flip _conjg;

        Spec() = delete;
        constexpr Spec(
            const size_t i,
            const vec_t g, const Flip a, const Flip c
        ) :
            ci { i }, gvec { g }, _anti{ a }, _conjg { c }
        {}
        constexpr Spec(
            const size_t i,
            const double gx, const double gy, const double gz,
            const Flip a, const Flip c
        ) :
            Spec(i,{gx,gy,gz},a,c)
        {}

        constexpr size_t idx() const { return ci; }
        constexpr const vec_t& wave_vector() const { return gvec; }
        constexpr double wave_vector(AnisoDirection d) const { return gvec[d]; }
        constexpr Flip anti() const { return _anti; }
        constexpr bool IsAnti() const { return (bool)_anti; }
        constexpr Flip conjg() const { return _conjg; }
        constexpr bool IsConj() const { return (bool)_conjg; }
    };


    class Container
    {
    public:

        using data_t = Spec;
        using dvec_t = std::vector<data_t>;
        using coeff_t = std::array<double,2>;

    private:

        coeff_t _coeff;
        dvec_t _vec;

    public:

        Container() = default;

        void reserve(size_t NC) { _vec.reserve(NC); }
        void push_back (data_t x) { _vec.push_back(x); }
        void push_back (
            const size_t i,
            const Spec::vec_t g, const Flip a, const Flip c
        ) {
            _vec.push_back(Spec(i,g,a,c));
        }
        void push_back (
            const size_t i,
            const double gx, const double gy, const double gz,
            const Flip a, const Flip c
        ) {
            _vec.push_back(Spec(i,gx,gy,gz,a,c));
        }
        void set (coeff_t x) { _coeff = x; }
        void set (size_t i, double x) { _coeff[i] = x; }

        size_t size() const { return _vec.size(); }

        double value() const { return _coeff[0]; }
        double phase() const { return _coeff[1]; }
        const data_t& key(size_t k) const
        {
            if ( k < size() ) {
                return _vec[k];
            } else {
                throw std::out_of_range( "bloch: out of range." );
            }
        }
        size_t idx(size_t k) const { return key(k).idx(); }
        Spec::vec_t wave_vector(size_t k) const
        {
            return key(k).wave_vector();
        }
        double wave_vector(size_t k,AnisoDirection d) const
        {
            return (wave_vector(k))[d];
        }
        Flip anti(size_t k) const { return key(k).anti(); }
        bool IsAnti(size_t k) const { return key(k).IsAnti(); }
        Flip conjg(size_t k) const { return key(k).conjg(); }
        bool IsConj(size_t k) const { return key(k).IsConj(); }

    };

}