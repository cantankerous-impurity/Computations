template< class T >
unsigned int NewtonMethErr(const T &Z)
{
    static const double log10_e = log10(E);
    static const double log10_2 = log10(2);
    static const double ep{ std::numeric_limits<double>::epsilon() };
    static const double err{ -log10(ep) };

    double zabs{ std::abs(Z) };
    T&& Z2{ .5*Z*Z };
    double log10_zabs{ log10(zabs) };

    // C == abs(exact) - abs(asymptot)
    // exact = exp(z^2/2)*( jn(q)/q^n )*z^(2n+1) 
    // asymptot = exp(z^2/2)*(2^(n)*exp(n+1)*n^(n+1/2)/(2n+1)^(2n+3/2))*z^(2n+1)
    // C = A * B^{n} * z(n)
    // A = exp( real( z^{2}/2+1 ) )*abs(z)
    // B = 2*exp(1)*abs(z)^{2}
    // z(n) = (n)^{n+1/2} / (2n+1)^{2n+3/2}

    // f(n) = log10(C) counts the approximate order of magnitude of C
    // f(n) = log10(A) + n*log10(B) + log10(z(n))

    // Use Newton's method to solve f(n) = fA + n*fB + f1(n) + error == 0
    // Z2 = z^2/2
    // fA = real( Z2+1 )*log10(e) + log10(zabs)
    // fB = log10(2) + log10(e) + 2*log10(zabs)
    // f1(n) = (n+1/2)*log10(n) - (2n+3/2)*log10(2n+1)

    // This makes the difference between the asymptotic expansion and the
    // function equal to 10^-error
    double fA{ std::real(Z2+1.)*log10_e + log10_zabs };
    double fB{ log10_2 + log10_e + 2*log10_zabs };
    auto F1 = [](int64_t n)->double
    {
        return (n+.5)*log10(n) - (2.*n+1.5)*log10(2.*n+1.);
    };
    auto F = [fA,fB,F1](int64_t n)->double
    {
        return fA + n*fB + F1(n) + err;
    };


    int n0{ (int)ceil(0.5*zabs*zabs) };
    double f0{ F(n0) };
    int n1{ n0+5 };
    double f1{ F(n1) };

    int n2;
    for( int i = 1; i<20; i++ )
    {
        n2 = ceil( n1 - ( n1-n0 )/( 1.-(f0/f1) ) );
        double f2{ F(n2) };

        if( n2 == n1 ) { break; }

        n0 = n1;
        f0 = f1;
        n1 = n2;
        f1 = f2;
    }

    return n2 + ceil(16*sqrt(n2)) + 1;
};


template < typename T > requires Complex<T>
class BuchholtzCoeffs
{

    static constexpr double ep{ std::numeric_limits<double>::epsilon() };

    unsigned int _k;
    unsigned int _N;
    T _Z;
    std::complex<double> _q;
    std::complex<double> cos_q;
    std::complex<double> sin_q;
    std::complex<double> _X;
    double _gf;

    std::valarray<std::complex<double>> _sB;
    std::vector<std::complex<double>> _f;

    std::vector<double> _d{ 1., 0., .25 };
    std::vector<double> _e{ 1., 0., .75 };

public:
    
    BuchholtzCoeffs() = delete;
    BuchholtzCoeffs(unsigned int k, unsigned int N, T Z)
        : _k{k}, _N{N}, _Z{Z},
        _q{ iunit*_Z*sqrt(2.*_k-1.) },
        cos_q{ std::cos(_q) }, sin_q{ std::sin(_q) },
        _X{ _Z*_Z/_q },
        _gf { boost::math::tgamma_ratio(.5*_k+.5,.5*_k) }
    {

        _d.resize(_N+1);
        _e.resize(_N);
        _f.resize(_N+1);
        _sB.resize(_N);

        {
            std::complex<double>&& g0{ ep/2. };
            std::complex<double>&& g1{ ep/.2 };
            for( unsigned int v = _N-1; v >= 3; v-- )
            {
                _sB[v] = ( 2.*v + 3. )*g1/_q - g0;
                g0 = g1;
                g1 = _sB[v];
            }

            std::complex<double>&& _q2{ _q*_q };
            std::complex<double>&& _q3{ _q*_q2 };

            _sB[2] = ( 2.*(2) + 3. )*g1/_q - g0;
            _sB *= ( -3.*cos_q/_q2 + (3.-_q2)*sin_q/_q3 )/( _sB[2] ) ;

            _sB[0] = sin_q/_q;
            _sB[1] = -cos_q/_q + sin_q/_q2;
        }


        for( unsigned int j=3; j < _N; j++ )
        {
            _d[j] = (j-1.5)*d(j-2) + (k-.5)*d(j-3);
            _e[j] = (j-.5)*e(j-2) + (k-.5)*e(j-3);
        }
        {
            unsigned int j{ _N };
            _d[j] = (j-1.5)*d(j-2) + (k-.5)*d(j-3);
        }        

        for( unsigned int j=0; j < _N; j++ )
        {
            _f[j] = ( d(j+1)*_Z - 2*_gf*e(j) )*_sB[j];
        }
    }

    double d(unsigned int i) const { return _d[i]; }
    double e(unsigned int i) const { return _e[i]; }
    T Z() const { return _Z; }
    std::complex<double> f(unsigned int i) const { return _f[i]; }
    std::complex<double> q() const { return _q; }
    std::complex<double> X() const { return _X; }
    std::complex<double> cos() const { return cos_q; }
    std::complex<double> sin() const { return sin_q; }

};




template < typename T > requires Complex<T>
std::complex<double> HyperBuch(const int j, const T &Z) {

    static constexpr double ep{ std::numeric_limits<double>::epsilon() };
    unsigned int nB{ NewtonMethErr(Z) };

    BuchholtzCoeffs cs(j,nB,Z);

    std::complex<double> prev{ cs.f(0) };
    std::complex<double> now{ prev + cs.f(1)*cs.X() };
    std::complex<double> nw{ ( cs.f(2)/cs.f(1) )*cs.X() };

    for( unsigned int v=3; v < nB; v++ )
    {
        bool abs_err{ std::abs( (now-prev)*nw ) <= ep };
        bool rel_err{ std::abs( 1. - prev/now ) <= ep };
        if( abs_err || rel_err ) { break; }

        auto tmp{ now };
        now = now + (now-prev)*nw;
        prev = tmp;

        nw = ( cs.f(v)/cs.f(v-1) )*cs.X();
    }

    return std::exp(.5*Z*Z)*( cs.cos() + now*Z );
}

template < typename T > requires Complex<T>
T HyperAsympt(const int j, const T &Z) {
    static const double ep{ std::numeric_limits<double>::epsilon() };
    uint_fast8_t del{ uint_fast8_t( (j+1)%2 ) };
    int num{ (int)floor((j+1)/2) };

    T part1{ 0. };
    T sum;
    if( sgn(std::real(Z))==-1 )
    {
        int k{ 0 };
        if( std::abs(Z) > .5 )
        {
            T nw {
                2*(sqrt(PI)/boost::math::tgamma(j/2.))
                *std::exp(Z*Z)*std::pow(-Z,j-1) 
            };
            sum = nw;
            while(
                ( std::abs(nw) > ep ) && ( std::abs(nw/sum) > ep )
                && ( k < num-1 )
            ) {
                k++;
                // T old{ nw };
                T mult{ ( (2.*k-j)*(2.*k-j-1.) )/( k*(2.*Z)*(2.*Z) ) };
                nw *= mult;
                sum += nw; 
            }
        } else {
            T nw {
                2*( del==0 ? 1 : -1 )
                *(1/boost::math::tgamma_delta_ratio(num,del/2.))
                *std::exp(Z*Z)
                *std::pow(2.*Z,del)
            };
            sum = nw;
            while(
                ( std::abs(nw) > ep ) && ( std::abs(nw/sum) > ep )
                && ( k < num-1 )
            ) {
                k++;
                // T old{ nw };
                T mult{ 
                    ( (num-k)*(2.*Z)*(2.*Z) )/( (2.*k+del-1.)*(2.*k+del) ) 
                };
                nw *= mult;
                sum += nw; 
            }
        }
        part1 = sum;
    }

    {
        sum = 2/std::pow(2.*Z,j)*boost::math::tgamma_ratio((double)j,j/2.);
        int k{ 1 };
        T mult{ ( -(2.*k+j-2.)*(2.*k+j-1.) )/( k*(2.*Z)*(2.*Z) ) };
        if( std::abs(mult) > 1. ) { throw; }
        T old{ sum };
        T nw{ mult*old };
        while(
            ( std::abs(nw) > ep ) && ( std::abs(nw/(part1+sum)) > ep )
            && ( k < 200 )
        ) {
            sum += nw;
            k++;
            old = nw;
            mult = ( -(2.*k+j-2.)*(2.*k+j-1.) )/( k*(2.*Z)*(2.*Z) );
            if( std::abs(mult) > 1. ) { throw; }
            nw *= mult;
        }

        return (part1+sum+nw);
    }
}

template < typename T > requires Complex<T>
std::complex<double> HyperFunc(int j, T &Z) {

  static const double log10_2 = log10(2);
  static const double log10_e = log10(E);
  static const double log10_pi = log10(PI);

  double ZAbs{ std::abs(Z) };
  int mi{ std::max(int(ceil(ZAbs*ZAbs-j+1.5))-1,1) };
  double log10_mi{ log10(mi) };

  double digits;
  digits = (j-2*mi-1)*log10(j+2*mi-2)-log10_mi;
  digits += -2*mi*(2*log10_2+log10_e+2*log10(ZAbs)+log10_mi);

  if (j == 1 || j == 2) {
    digits += -(2*j-3)*log10_e;
  } else {
    digits += -log10_2-log10_pi+(1-j)*log10(j-2)-j*log10(j-1);
  }

  digits *= 0.5;

  if (digits > -16) {
    return HyperBuch(j,Z);
  } else {
    return HyperAsympt(j,Z);
  }

}