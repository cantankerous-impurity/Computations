namespace Mesh
{

    using si_t = int;
    using ui_t = unsigned int;
    using csi_t = const si_t;

    constexpr ui_t MaxK{ 10U };

    constexpr auto OneSize{ MaxK+3 };
    template < typename T >
    class One
    {
        std::array<T,OneSize> _data{ 0. };
        using ui_t = std::remove_const_t<decltype(OneSize)>;

    public:
        One() = default;
        void reset() { _data.fill(0.); }
        constexpr auto size() const { return OneSize; }
        void zero( const ui_t K )
        {
            const ui_t _K{ K+1 };
            if( size() < _K )
            {
                throw std::length_error( "Not enough space for OneOne." );
            }
            for ( ui_t k = 0; k < _K; k++ ) { (*this)[k] = 0; }
        }
        
        T& operator[](auto i) { return _data[i]; }
        const T& operator[](auto i) const { return _data[i]; }

        T& operator()(auto u) { return _data[u]; }
        const T& operator()(auto u) const { return _data[u]; }

        One& operator+=(const One& x)
        {
            if ( _data.size() < x.size() ) { throw; }
            for (auto i=0; i<x.size(); i++) { (*this)(i) += x(i); }
            return *this;
        }
        One& operator*=(const double x)
        {
            for (auto i=0; i<size(); i++) { (*this)(i) *= x; }
            return *this;
        }
        One& operator*=(const Flip x)
        {
            for (auto i=0; i<size(); i++) { (*this)(i) *= (double)x; }
            return *this;
        }
    };

    constexpr auto TwoSize{ OneSize*OneSize };
    template < typename T >
    class Two
    {
        std::array<T,TwoSize> _data{ 0. };
        using ui_t = std::remove_const_t<decltype(TwoSize)>;
        ui_t _N;
        bool RowMajor;

    public:
        Two() = default;
        void resize(ui_t NR, ui_t NC)
        {
            _N = std::max(NR,NC);
            // if (NR > NC) { RowMajor = true; } else { RowMajor = false; }
            if (NR <= NC) { RowMajor = true; } else { RowMajor = false; }
        }
        void reset() { _data.fill(0.); }
        void zero( const ui_t K0, const ui_t K1 )
        {
            const ui_t _K{ (K0+1)*(K1+1) };
            if( (*this).size() < _K )
            {
                throw std::length_error( "Not enough space for TwoMesh." );
            }
            for ( ui_t k = 0; k < _K; k++ ) { (*this)[k] = 0; }
        }
        void transpose() { RowMajor = !RowMajor; }

        constexpr auto size() const { return TwoSize; }

        auto ix_RM(ui_t t, ui_t u) const { return u+_N*t; }
        auto ix_CM(ui_t t, ui_t u) const { return t+_N*u; }
        auto ix(ui_t t, ui_t u) const
        {
            return (RowMajor ? ix_RM(t,u) : ix_CM(t,u));
        }

        T& operator[](const ui_t i) { return _data[i]; }
        const T& operator[](const ui_t i) const { return _data[i]; }
        
        T& operator()(const ui_t t, const ui_t u)
        {
            return (*this)[ix(t,u)];
        }
        const T& operator()(const ui_t t, const ui_t u) const
        {
            return (*this)[ix(t,u)];
        }

        Two& operator+=(const Two& x)
        {
            if ( _data.size() < x.size() ) { throw; }
            for (ui_t i=0; i<x.size(); i++) { (*this)[i] += x[i]; }
            return *this;
        }
        Two& operator*=(const double x)
        {
            for (ui_t i=0; i<size(); i++) { (*this)[i] *= x; }
            return *this;
        }
        Two& operator*=(const Flip x)
        {
            for (ui_t i=0; i<size(); i++) { (*this)[i] *= (double)x; }
            return *this;
        }
    };

    constexpr auto OneOneSize{ (OneSize*(OneSize+1))/2 };
    template < typename T >
    class OneOne
    {
        std::array<T,OneOneSize> _data{ 0. };
        using ui_t = std::remove_const_t<decltype(OneOneSize)>;

    public:
        OneOne() = default;
        void reset() { _data.fill(0.); }
        constexpr auto size() const { return OneOneSize; }
        void zero( const ui_t K )
        {
            const ui_t _K{ ((K+1)*(K+2))/2 };
            if( size() < _K )
            {
                throw std::length_error( "Not enough space for OneOne." );
            }
            for ( ui_t k = 0; k < _K; k++ ) { (*this)[k] = 0; }
        }

        static auto ix(auto u, auto v) { return u + (v*(v-1))/2; }

        T& operator[](const auto i) { return _data[i]; }
        const T& operator[](const auto i) const { return _data[i]; }
        
        T& operator()(const auto u, const auto w)
        {
            return (*this)[ix(u,w)];
        }
        const T& operator()(const auto u, const auto w) const
        {
            return (*this)[ix(u,w)];
        }

        OneOne& operator+=(const OneOne& x)
        {
            if ( _data.size() < x.size() ) { throw; }
            for (auto i=0; i<x.size(); i++) { (*this)[i] += x[i]; }
            return *this;
        }
        OneOne& operator*=(const double x)
        {
            for (auto i=0; i<size(); i++) { (*this)[i] *= x; }
            return *this;
        }
        OneOne& operator*=(const Flip x)
        {
            for (auto i=0; i<size(); i++) { (*this)[i] *= (double)x; }
            return *this;
        }
    };

    constexpr auto TwoOneSize{ TwoSize*OneSize };
    template < typename T >
    class TwoOne
    {
        std::array<T,TwoOneSize> _data { 0. };
        using ui_t = std::remove_const_t<decltype(TwoOneSize)>;
        ui_t _N;

    public:
        TwoOne() = default;
        void resize(auto NR, auto NC) { _N = std::max(NR,NC); }
        constexpr auto size() const { return TwoOneSize; }
        void zero( const auto K0, const auto K1 )
        {
            const auto _K{ ((K0+1)*(K1+1)*(K0+K1+2))/2 };
            if( (*this).size() < _K )
            {
                throw std::length_error( "Not enough space for TwoMesh." );
            }
            for ( ui_t k = 0; k < _K; k++ ) { (*this)[k] = 0; }
        }

        auto index(auto u0, auto u1, auto v) const
        {
            auto U{ u0+u1 };
            auto X0{ ( U*(U+1) )/2 + u0 };
            auto X1{ ( U*(U+1)*(2*U+1) ) / 6 };
            return X0*_N - X1 - u0*(U+1) + v;
        }

        T& operator[](const auto t) { return _data[t]; }
        const T& operator[](const auto t) const { return _data[t]; }

        T& operator()(auto u1, auto u2, auto w)
        {
            return (*this)[index(u1,u2,w)];
        }
        const T& operator()(auto u1, auto u2, auto w) const
        {
            return (*this)[index(u1,u2,w)];
        }
        
    };

    template< class T >
    class Kin
    {
        std::array<T*,3> _data;

    public:
        Kin() = delete;
        Kin(T& J0, T& J1): _data{&J1,&J0,&J0} {}
        Kin(std::array<T,3>& in): _data{&(in[0]),&(in[1]),&(in[2])} {}
        Kin(std::array<T,3>& in, const std::array<T,3>&& _X): Kin{in}
        {
            operator=(_X);
        }

        T& operator[](ui_t t) { return *(_data[t]); }
        const T& operator[](ui_t t) const { return *(_data[t]); }
        T& operator()(csi_t s) { return *(_data[s+1]); }
        const T& operator()(csi_t s) const { return *(_data[s+1]); }
        template< class... Types>
        auto& operator()(csi_t s, Types... args) { return (*this)(s)(args...); }
        template< class... Types>
        const auto& operator()(csi_t s, Types... args) const
        {
            return (*this)(s)(args...);
        }

        Kin<T>& operator=(const std::array<T,3>& _X)
        {
            // for (ui_t t=0; t<3; t++) { operator[](t) = _X[t]; }
            constexpr_for<(ui_t)0,(ui_t)3,(ui_t)1>([&](auto t)
            {
                (*this)[t] = _X[t];
            });
            return *this;
        }

        Kin<T>& operator+=(const Kin<T>& x)
        {
            // for (si_ts=-1; s<2; s++)
            // {
            //     operator()(s) += x(s);
            // }
            constexpr_for<(ui_t)0,(ui_t)3,(ui_t)1>([&](auto t)
            {
                (*this)(t-1) += x(t-1);
            });
            return *this;
        }
        Kin<T>& operator*=(const Flip x)
        {
            // for (ui_t t=0; t<3; t++)
            // {
            //     operator[](t) *= (double)x;
            // }
            constexpr_for<(ui_t)0,(ui_t)3,(ui_t)1>([&](auto t)
            {
                (*this)[t] *= (double)x;
            });
            return *this;
        }
    };

    template< class T >
    class KinKin
    {
        std::array<T*,9> _data;

    public:
        KinKin() = delete;
        KinKin(std::array<T,3>& J): _data{
            &J[2],&J[1],&J[1],
            &J[1],&J[0],&J[0],
            &J[1],&J[0],&J[0]
        } {}
        KinKin(std::array<T,9>& in): _data{
            &(in[0]),&(in[1]),&(in[2]),
            &(in[3]),&(in[4]),&(in[5]),
            &(in[6]),&(in[7]),&(in[8])
        } {}
        KinKin(std::array<T,9>& in, const std::array<T,9>&& _X): KinKin{in}
        {
            // for ( ui t=0; t<9; t++ )
            // {
            //     operator[](t) = _X[t];
            // }
            constexpr_for<(ui_t)0,(ui_t)9,(ui_t)1>([&](auto t)
            {
                operator[](t) = _X[t];
            });
        }

        static ui_t ix(csi_t s0, csi_t s1) { return s0+3*s1+4; }

        T& operator[](ui_t t) { return *(_data[t]); }
        T& operator()(csi_t s0, csi_t s1) { return (*this)[ix(s0,s1)]; }
        const T& operator[](ui_t t) const { return *(_data[t]); }
        const T& operator()(csi_t s0, csi_t s1) const
        {
            return (*this)[ix(s0,s1)];
        }
        template< class... Types>
        auto& operator()(csi_t s0, csi_t s1, Types... args)
        {
            return (*this)(s0,s1)(args...);
        }
        template< class... Types>
        const auto& operator()(csi_t s0, csi_t s1, Types... args) const
        {
            return (*this)(s0,s1)(args...);
        }

        KinKin<T>& operator=(const std::array<T,9>& _X)
        {
            // for (ui_t t=0; t<9; t++)
            // {
            //     operator[](t) = _X[t];
            // }
            constexpr_for<(ui_t)0,(ui_t)9,(ui_t)1>([&](auto t)
            {
                (*this)[t] = _X[t];
            });
            return *this;
        }
        KinKin<T>& operator+=(const KinKin<T>& x)
        {
            // for (si_t j=-1; j<2; j++)
            // {
            //     for (si_t i=-1; i<2; i++)
            //     {
            //         operator()(i,j) += x(i,j);
            //     }
            // }
            constexpr_for<(ui_t)0,(ui_t)9,(ui_t)1>([&](auto t)
            {
                constexpr si_t s0{ (t%3) - 1 };
                constexpr si_t s1{ (t/3) - 1 };
                (*this)[t] += x(s0,s1);
            });
            return *this;
        }
        KinKin<T>& operator*=(const Flip x)
        {
            // for (ui_t t=0; t<9; t++)
            // {
            //     operator[](t) *= (double)x;
            // }
            constexpr_for<(ui_t)0,(ui_t)9,(ui_t)1>([&](auto t)
            {
                (*this)[t] *= (double)x;
            });
            return *this;
        }
    };




}





