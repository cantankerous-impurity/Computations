namespace Matrix{

    class Manager
    {
    public:
        
        using ElemSet_t = std::array<Elem,NumElems>;
        ElemSet_t BasisSet;

    private:

        std::array<cmplx, NumElems*NumOperators*NumOpTerms> op_data { 0. };

        Mat Ovr;
        Mat Ham;
        Mat HH;

        EPS eps;

        std::array<std::array<double,2>,NumStates> Energies;
        PetscReal del;

    public:

        template<size_t... iE>
        ElemSet_t BuildElements(indices_pack<iE...>){
            return {{ Elem(iE,ElementValues(iE))... }};
        }
        template< size_t N >
        ElemSet_t BuildElements(){
            return BuildElements(idx_range<N>{});
        }

        ElemValues ElementValues(size_t iE)
        {
            ElemValues res;
            for( int i=0; i<NumOperators; i++ )
            {
                for( int j=0; j<NumOpTerms; j++)
                {
                    size_t k { j + NumOpTerms*i + NumOperators*NumOpTerms*iE };
                    res[i][j] = &op_data[k];
                }
            }
            return res;
        }

        Manager() : BasisSet{ BuildElements<NumElems>() }
        {

            PetscCallVoid(MatCreate(PETSC_COMM_WORLD,&Ovr));
            PetscCallVoid(MatSetSizes(Ovr,PETSC_DECIDE,PETSC_DECIDE,NS(),NS()));
            PetscCallVoid(MatSetFromOptions(Ovr));
            PetscCallVoid(MatSetUp(Ovr));

            PetscCallVoid(MatCreate(PETSC_COMM_WORLD,&Ham));
            PetscCallVoid(MatSetSizes(Ham,PETSC_DECIDE,PETSC_DECIDE,NS(),NS()));
            PetscCallVoid(MatSetFromOptions(Ham));
            PetscCallVoid(MatSetUp(Ham));

            PetscCallVoid(MatCreate(PETSC_COMM_WORLD,&HH));
            PetscCallVoid(MatSetSizes(HH,PETSC_DECIDE,PETSC_DECIDE,NS(),NS()));
            PetscCallVoid(MatSetFromOptions(HH));
            PetscCallVoid(MatSetUp(HH));

            PetscCallVoid(EPSCreate(PETSC_COMM_WORLD,&eps));
            PetscCallVoid(EPSSetWhichEigenpairs(eps,EPS_SMALLEST_REAL));

            Compute();
            EigenSolve();

        }

        template< size_t idx >
        void Set(double r0=1., double a0=1.)
        {
            Basis::CurrBases.Set<idx>(r0,a0);
            op_data.fill( 0. );
            PetscCallVoid(EmptyPetscMat(Ovr));
            PetscCallVoid(EmptyPetscMat(Ham));
            PetscCallVoid(EmptyPetscMat(HH));
            Compute();
            EigenSolve();
        }

        void Compute(size_t i) { BasisSet[i].Compute(); }

        void Compute()
        {
            for (size_t i = 0; i < NE(); i++) { Compute(i); }

            PetscCallVoid(FillPetscMat(Ovr,std::array{Operator::Overlap}));
            PetscCallVoid(FillPetscMat(Ham,Operator::HamOrder));
            PetscCallVoid(FillPetscMat(HH,Operator::HHOrder));

            if constexpr( !WillDoLoop )
            {
                
                PetscCallVoid(ViewOvr());
                PetscCallVoid(ViewHam());
                PetscCallVoid(ViewHH());
            }

            Mat Del;

            PetscCallVoid(MatCreate(PETSC_COMM_WORLD,&Del));
            PetscCallVoid(MatSetSizes(Del,PETSC_DECIDE,PETSC_DECIDE,NS(),NS()));
            PetscCallVoid(MatSetFromOptions(Del));
            PetscCallVoid(MatSetUp(Del));

            PetscCallVoid(MatAssemblyBegin(Del,MAT_FINAL_ASSEMBLY));
            PetscCallVoid(MatAssemblyEnd(Del,MAT_FINAL_ASSEMBLY));
            PetscCallVoid(MatCopy(Ovr, Del, UNKNOWN_NONZERO_PATTERN));
            PetscCallVoid(MatShift(Del,-1.));

            PetscCallVoid(MatNorm(Del,NORM_FROBENIUS,&del));

            PetscCallVoid(MatDestroy(&Del));

        }

        void EigenSolve() {

            for( size_t i=0; i<NumStates; i++ )
            {
                Energies[i][0] = std::numeric_limits<double>::quiet_NaN();
                Energies[i][1] = std::numeric_limits<double>::quiet_NaN();
            }
            
            if ( PetscEigenSolve() == PETSC_SUCCESS )
            {
                PetscInt nconv;
                PetscCallVoid(EPSGetConverged(eps,&nconv));
                if (nconv>0)
                {
                    {
                        BV bv;
                        PetscCallVoid(EPSGetBV(eps,&bv));
                        PetscCallVoid(BVSetMatrix(bv,Ovr,PETSC_FALSE));
                    }

                    for (PetscInt i=0; i<nconv; i++)
                    {
                        Vec x;
                        PetscCallVoid(MatCreateVecs(Ham,nullptr,&x));
                        PetscScalar Eng;
                        PetscCallVoid(EPSGetEigenpair(
                            eps,i,&Eng,nullptr,x,nullptr
                        ));

                        // PetscReal error;
                        // PetscCall(EPSComputeError(
                        //     eps,i,EPS_ERROR_RELATIVE,&error
                        // ));

                        if (PetscImaginaryPart(Eng)!=0.) {
                            std::cout << "\n Warning. Imaginary eigenvalue.\n";
                        }
                        Energies[i][0] = PetscRealPart(Eng);
                        

                        PetscScalar v;PetscScalar w;
                        Vec HHx;
                        Vec Ovrx;
                        PetscCallVoid(MatCreateVecs(HH,nullptr,&HHx));
                        PetscCallVoid(MatCreateVecs(Ovr,nullptr,&Ovrx));
                        MatMult(HH,x,HHx);
                        MatMult(Ovr,x,Ovrx);
                        VecDot(HHx,x,&v);
                        VecDot(Ovrx,x,&w);
                        PetscCallVoid(VecDestroy(&HHx));
                        PetscCallVoid(VecDestroy(&x));
                        
                        Energies[i][1] = real( (v/w)/(Eng*Eng) - 1 );
                    }
                }
            }
            else
            {
                // PetscCallVoid(EPSDestroy(&eps));
                PetscCallVoid(EPSCreate(PETSC_COMM_WORLD,&eps));
                PetscCallVoid(EPSSetWhichEigenpairs(eps,EPS_SMALLEST_REAL));
            }
        }

        void Finalize()
        {
            PetscCallVoid(MatDestroy(&Ovr));
            PetscCallVoid(MatDestroy(&Ham));
            PetscCallVoid(MatDestroy(&HH));
            PetscCallVoid(EPSDestroy(&eps));
        }

        PetscErrorCode PetscEigenSolve()
        {
            if constexpr( !WillDoLoop )
            {
                
                PetscCall(ViewOvr());
                PetscCall(ViewHam());
                PetscCall(ViewHH());
            }

            PetscCall(EPSSetOperators(eps,Ham,Ovr));
            PetscCall(EPSSetProblemType(eps,EPS_GHEP));
            PetscCall(EPSSetDimensions(eps,NumStates,3*NumStates,3*NumStates));
            PetscCall(EPSSetTolerances(eps,1.e-15,PETSC_DEFAULT));

            PetscCall(EPSSetFromOptions(eps));
            {
                BV bv;
                PetscCall(EPSGetBV(eps,&bv));
                PetscCall(BVSetDefiniteTolerance(bv,1.e-5));
            }

            PetscCall(EPSSolve(eps));
            return PETSC_SUCCESS;
        }

        template< class T > 
        PetscErrorCode FillPetscMat(Mat& M, T&& Ops)
        {
            PetscInt IStart,IEnd;
            PetscCall(MatGetOwnershipRange(M,&IStart,&IEnd));

            for( auto oS : Ops )
            {
                for( PetscInt i=IStart; i<IEnd; i++ )
                {
                    {
                        PetscScalar R{ value(PackIdxs(i,i),oS) };
                        PetscCall(MatSetValue(M,i,i,R,ADD_VALUES));

                        for( size_t j = i+1; j<NS(); j++ )
                        {
                            PetscScalar R{ value(PackIdxs(i,j),oS) };
                            PetscScalar Rc{ std::conj(R) };
                            PetscCall(MatSetValue(M,i,j,R,ADD_VALUES));
                            PetscCall(MatSetValue(M,j,i,Rc,ADD_VALUES));
                        }
                    }
                }
            }
            PetscCall(MatAssemblyBegin(M,MAT_FINAL_ASSEMBLY));
            PetscCall(MatAssemblyEnd(M,MAT_FINAL_ASSEMBLY));

            return 0;
        }

        PetscErrorCode EmptyPetscMat(Mat& M)
        {
            PetscInt IStart,IEnd;
            PetscCall(MatGetOwnershipRange(M,&IStart,&IEnd));

            for( PetscInt i=IStart; i<IEnd; i++ )
            {
                {
                    // PetscScalar R{ 0. };
                    PetscCall(MatSetValue(M,i,i,0.,INSERT_VALUES));

                    for( size_t j = i+1; j<NS(); j++ )
                    {
                        PetscScalar R{ 0. };
                        PetscCall(MatSetValue(M,i,j,0.,INSERT_VALUES));
                        PetscCall(MatSetValue(M,j,i,0.,INSERT_VALUES));
                    }
                }
            }
            PetscCall(MatAssemblyBegin(M,MAT_FINAL_ASSEMBLY));
            PetscCall(MatAssemblyEnd(M,MAT_FINAL_ASSEMBLY));

            return 0;
        }

        PetscErrorCode ViewOvr()
        {
            return MatView(Ovr,PETSC_VIEWER_STDOUT_WORLD);
        }
        PetscErrorCode ViewHam()
        {
            return MatView(Ham,PETSC_VIEWER_STDOUT_WORLD);
        }
        PetscErrorCode ViewHH()
        {
            return MatView(HH,PETSC_VIEWER_STDOUT_WORLD);
        }

        size_t NS() const { return NumStates; }
        size_t NE() const { return NumElems; }

        std::complex<double> value(size_t iE,Operator::Spec oS,size_t i=6) const 
        {
            return BasisSet[iE].Value(oS,i);
        }

        EPS Eigensystem() { return eps; }

        PetscErrorCode NumberIts()
        {
            PetscInt its;
            PetscCall(EPSGetIterationNumber(eps,&its));
            PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                " Number of iterations of the method: %d\n",its
            ));
            return 0;
        }
        PetscErrorCode SolutionMethod()
        {
            EPSType type;
            PetscCall(EPSGetType(eps,&type));
            PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                " Solution method: %s\n\n",type
            ));
            return 0;
        }
        PetscErrorCode NumReqEigenvalues()
        {
            PetscInt nev;
            PetscCall(EPSGetDimensions(eps,&nev,nullptr,nullptr));
            PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                " Number of requested eigenvalues: %d\n",nev
            ));
            return 0;
        }
        PetscErrorCode StoppingCondition()
        {
            PetscReal tol;
            PetscInt maxit;
            PetscCall(EPSGetTolerances(eps,&tol,&maxit));
            PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                " Stopping condition: tol=%.4g, maxit=%d\n",(double)tol,maxit
            ));
            return 0;
        }
        PetscErrorCode EigenSpectrum()
        {
            PetscInt nconv;
            PetscCall(EPSGetConverged(eps,&nconv));
            PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                " Number of converged eigenpairs: %d\n\n",nconv
            ));

            double eng0 { SystemUnits::Energy*1e3 };

            if( nconv>0 )
            {
                PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                    "      e(meV)              var       \n"
                    "----------------- ------------------\n"
                ));
                for( PetscInt i=0; i<nconv; i++ )
                {
                    double var{ Variance(i) };
                    double eng{ Energy(i) };
                    // if( var < 1 && eng < 0 )
                    if( eng < 0 )
                    {
                        PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                            "%12f         %12g\n",
                        eng*eng0,        var
                        ));
                    }                
                }
                PetscCall(PetscPrintf(PETSC_COMM_WORLD,"\n"));
            }
            return 0;
        }

        double Energy(size_t S) const { return Energies[S][0]; }
        double Variance(size_t S) const { return Energies[S][1]; }
        double Del() const { return del; }
    };

}