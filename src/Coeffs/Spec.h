namespace Coeff
{
    using si_t = int_fast8_t;
    using ui_t = uint_fast8_t;

    namespace Index
    {
        enum Enum { G, H, I, J, g, h, i, j };

        
        struct Spec {
            static constexpr ui_t En(Decay::Type D, bool SR)
            {
                bool SlaterZero{ !D.HasYukawaDecay() };
                bool GaussZero{ !D.HasGaussianDecay() };
                int en = 0;

                if (SR) { en += 1; }
                if constexpr(Basis::WfIsSlater)
                {
                    if (GaussZero) { en += 4; };
                    if (SlaterZero) { en += 2; };
                }
                if constexpr(Basis::WfIsGauss)
                {
                    if (GaussZero) { en += 2; };
                    if (SlaterZero) { en += 4; };
                }

                return en;
            }

            static constexpr ui_t nidxs (Enum in) {
                switch (in)
                {
                    case G : return 3;
                    case H : return 2;
                    case I : return 2;
                    case J : return 1;

                    case g : return 2;
                    case h : return 1;
                    case i : return 1;
                    case j : return 0;
                };
            }
            static constexpr auto name(Enum in)
            {
                switch (in)
                {
                    case G : return "G";
                    case H : return "H";
                    case I : return "I";
                    case J : return "J";
                    case g : return "g";
                    case h : return "h";
                    case i : return "i";
                    case j : return "j";
                };
            }
            static constexpr auto table(Enum in)
            {
                switch (in)
                {
                    case G : return "G";
                    case H : return "HI";
                    case I : return "HI";
                    case J : return "J";
                    case g : return "GContracted";
                    case h : return "HIContracted";
                    case i : return "HIContracted";
                    case j : return "JContracted";
                };
            }
            static constexpr auto vals(Enum in)
            {
                switch (in)
                {
                    case G : return "G_n";
                    case H : return "H_n";
                    case I : return "I_n";
                    case J : return "J_n";
                    case g : return "g_n";
                    case h : return "h_n";
                    case i : return "i_n";
                    case j : return "jn";
                };
            }
            static constexpr auto idxs(Enum in)
            {
                switch (in)
                {
                    case G : return "u1,u2,w"; // 7
                    case H : return "u,w";     // 3
                    case I : return "u,w";     // 3
                    case J : return "w";       // 1
                    case g : return "u1,u2";   // 5
                    case h : return "u";       // 1
                    case i : return "u";       // 1
                    case j : return "";        // 0
                };
            }


            const Enum value;


            Spec() = delete;
            constexpr Spec(Enum in) : value{ in } {}
            constexpr Spec(ui_t en) : Spec(static_cast<Enum>(en)) {}
            constexpr Spec(Decay::Type D, bool SR) : Spec(En(D,SR)) {}

            
            // Allow switch and comparisons.
            constexpr operator Enum() const { return value; }

            // Prevent usage: if(Spec)
            explicit operator bool() = delete;

            constexpr auto nidxs() const { return nidxs(value); }
            constexpr auto name() const { return name(value); }
            constexpr auto table() const { return table(value); }
            constexpr auto vals() const { return vals(value); }
            constexpr auto idxs() const { return idxs(value); }
        };

    }




    template< Kinetic::Spec K, size_t ex, Decay::Type D, bool SR > 
    struct Spec 
    {
        static constexpr auto I{ Index::Spec(D,SR) };
        static constexpr auto nidxs{ K.nidxs() + I.nidxs() };
        
        static constexpr auto _name()
        {
            constexpr std::string_view i{ I.name() };
            constexpr std::string_view k{ K.name() };

            constexpr size_t N{ i.size() + k.size() + 1 };
            std::array<char,N> s{ 0 };

            size_t n = 0;
            for( auto c : i) { s[n++] = c; }
            s[n++] = '_';
            for( auto c : k) { s[n++] = c; }

            return s;
        }
        static constexpr auto _table()
        {
            constexpr std::string_view i{ I.table() };
            constexpr std::string_view w{ WfType.name(WfType) };
            constexpr std::string_view k{ K.name() };
            constexpr std::string_view p{ Potential::Type::id(ex) };

            constexpr auto N{ i.size() + w.size() + k.size() + p.size() + 3 };
            std::array<char,N> s{ 0 };

            size_t n = 0;
            for( auto c : i) { s[n++] = c; }
            s[n++] = '_';
            for( auto c : w) { s[n++] = c; }
            s[n++] = '_';
            for( auto c : k) { s[n++] = c; }
            s[n++] = '_';
            for( auto c : p) { s[n++] = c; }

            return s;
        }
        static constexpr auto _cols()
        {
            constexpr std::string_view v{ I.vals() };
            size_t n = 0;
            if constexpr(nidxs==0)
            {
                constexpr size_t N { v.size() };
                std::array<char,N> s{ 0 };

                for( auto c : v) { s[n++] = c; }

                return s;
            }
            else
            {
                constexpr std::string_view k{ K.idxs() };
                constexpr std::string_view i{ I.idxs() };

                constexpr size_t N{
                    k.size() + i.size() + v.size()
                    + (k.size() > 0 ? 1 : 0)
                    + (i.size() > 0 ? 1 : 0)
                };
                std::array<char,N> s{ 0 };

                for( auto c : k) { s[n++] = c; }
                if(k.size() > 0) { s[n++] = ','; }
                for( auto c : i) { s[n++] = c; }
                if(i.size() > 0) { s[n++] = ','; }
                for( auto c : v) { s[n++] = c; }

                return s;
            }
        }
        static constexpr auto name{ _name() };
        static constexpr auto table{ _table() };
        static constexpr auto cols{ _cols() };

        constexpr Spec() = default;
        constexpr ~Spec() = default;

        // Allow switch and comparisons.
        constexpr operator Kinetic::Spec::Enum() const { return K; }
        constexpr operator Decay::Type() const { return D; }
        constexpr operator Index::Enum() const { return I; }

        // Prevent usage: if(Spec)
        explicit operator bool() = delete;
    };
}