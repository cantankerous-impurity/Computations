#===============================================================================
#
#	Prologue
#
#===============================================================================

# Refs
# https://docs.cloudposse.com/reference/best-practices/make-best-practices/
# https://danyspin97.org/blog/makefiles-best-practices/
# Stackoverflow: https://bit.ly/3sDaMQN
# https://www.gnu.org/prep/standards/html_node/Standard-Targets.html
# https://blog.horejsek.com/makefile-with-python/


SHELL := /bin/bash
COMPILER := gnu


ifneq (,$(wildcard ./host.mk))
    include ./host.mk
endif

#	Various directories within project
#-------------------------------------------------------------------------------


### SUBPROJECT DIRECTORIES
COMPUTE_DIR := ${PROJECT_DIR}/${COMPUTATION_SUBPROJECT}
PRECALCULATION_DIR := ${PROJECT_DIR}/${PRECALC_SUBPROJECT}


### SOURCE CODE DIRECTORIES
SRC_DIR        := ${COMPUTE_DIR}/src
PARAMS_DIR     := ${SRC_DIR}/Parameters
SPEC_DIR       := ${SRC_DIR}/Specification
OPERATORS_DIR  := ${SPEC_DIR}/Operators
BASIS_DIR      := ${SPEC_DIR}/Basis
RESULT_DIR     := ${SPEC_DIR}/Results
COEFFS_DIR     := ${SRC_DIR}/Coeffs
BLOCH_DIR      := ${SRC_DIR}/Bloch
INTEG_DIR      := ${SRC_DIR}/Integrand
PUNCTURE_DIR   := ${SRC_DIR}/Puncture
QUADRATURE_DIR := ${SRC_DIR}/Quadrature
CUBATURE_DIR   := ${SRC_DIR}/Cubature
MATRIX_DIR     := ${SRC_DIR}/Matrix
SQL_DIR        := ${SRC_DIR}/SQLite
VIZ_DIR        := ${SRC_DIR}/Visualization

### LIBRARY DIRECTORY
PETSC_ARCH_DIR := ${PETSC_DIR}/${PETSC_ARCH}
SLEPC_ARCH_DIR := ${SLEPC_DIR}/${PETSC_ARCH}

export PYTHONPATH  := ${PYTHONPATH}:${PRECALCULATION_DIR}:${SPEC_DIR}
export opPath      := ${OPERATORS_DIR}
export basisPath   := ${BASIS_DIR}
export coeffPath   := ${COEFFS_DIR}
export blochPath   := ${BLOCH_DIR}
export resultPath  := ${RESULT_DIR}

export dataDir       := ${PROJECT_DATA_DIR}
export precDataDir   := ${PROJECT_DATA_DIR}/precalc-coeffs
export blochDataDir  := ${PROJECT_DATA_DIR}/bloch
export resultDataDir := ${PROJECT_DATA_DIR}/results


#	Build variables
#-------------------------------------------------------------------------------

MPICXX := ${PETSC_ARCH_DIR}/bin/mpicxx

CXX.gnu := /usr/bin/g++
CC.gnu := /usr/bin/gcc
LD.gnu := /usr/bin/g++

CXX.clang := /usr/bin/clang++
CC.clang := /usr/bin/clang
LD.clang := /usr/bin/clang++

CXX := ${MPICXX}
# CXX ?= ${CXX.${COMPILER}}


CC ?= ${CC.${COMPILER}}
LD ?= ${LD.${COMPILER}}
AR ?= /bin/ar


# CXXFLAGS := ${CXXFLAGS}

# CXXFLAGS.gnu.debug += -ggdb -Og
# # CXXFLAGS.gcc.debug := -ggdb -O0
# CXXFLAGS.gnu.release := -O3
# CXXFLAGS.gnu := ${CXXFLAGS.${COMPILER}.${BUILD}}
# CXXFLAGS += ${CXXFLAGS.${COMPILER}}

# clang/gcc version must be greater than 10 to use c++20
CXXVER_GTEQ_10 := ${shell echo $\
	$$( ver=$$(exec 2> /dev/null; ${CXX} -dumpversion); $\
	if [[ $$ver ]]; then echo $$(( $$ver>=10 )); else echo "0"; fi )}
ifeq (${CXXVER_GTEQ_10} , 1)
    CXXFLAGS += -std=c++20
endif


CXXFLAGS += -f{diagnostics-color=always,strict-enums}
CXXFLAGS += -W{all,error,extra,pedantic}
# CXXFLAGS += -I${BOOST_DIR} -L${BOOST_DIR}/libs
CXXFLAGS += -I${CUBA_DIR} -L${CUBA_DIR}
CXXFLAGS += -I{${PETSC_ARCH_DIR}/include,${PETSC_DIR}/include}
CXXFLAGS += -L{${PETSC_ARCH_DIR}/lib,${PETSC_DIR}/lib}
CXXFLAGS += -Wl,-rpath,${PETSC_ARCH_DIR}/lib
CXXFLAGS += -I{${SLEPC_ARCH_DIR}/include,${SLEPC_DIR}/include}
CXXFLAGS += -L{${SLEPC_ARCH_DIR}/lib,${SLEPC_DIR}/lib}
CXXFLAGS += -Wl,-rpath,${SLEPC_ARCH_DIR}/lib
CXXFLAGS += -ftemplate-depth=5000


#===============================================================================
#
#	Recipes
#
#===============================================================================

.PHONY: default all deps build install help clean

default : help

all:

deps:

build:

install:

help:
# 	@printf "Available targets:\n\n"
# 	@awk '/^[a-zA-Z\-\_0-9%:\\]+/ { \
# 	    helpMessage = match(lastLine, /^## (.*)/); \
# 	    if (helpMessage) { \
# 	        helpCommand = $$1; \
# 	        helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
# 	        gsub("\\\\", "", helpCommand); \
# 	        gsub(":+$$", "", helpCommand); \
# 	        printf "  \x1b[32;01m%-35s\x1b[0m %s\n", \
# 	        helpCommand, helpMessage; \
# 	    } \
# 	} \
#     { lastLine = $$0 }' ${MAKEFILE_LIST} | sort -u @printf "\n

clean:
# 	-find ${PROJECT_DIR} -type f -name "*.o" -delete
# 	-find ${PROJECT_DIR} -type f -name "*.a" -delete
# 	-find ${PROJECT_DIR} -type f -name "*.so" -delete
# 	-find ${PROJECT_DIR} -type f -name "*.mod" -delete
# 	-find ${PROJECT_DIR} -type f -name "*.smod" -delete
# 	-find ${PROJECT_DIR} -type d -empty -delete


.PHONY: docker.build.remote docker.build.local docker.push 



docker.build.remote:
	docker build -t lpendo/cantankerous-impurity .

docker.build.local:
	docker build -t cantankerous-impurity .

docker.push:
	docker push lpendo/cantankerous-impurity


.PHONY: docker.pull

docker.pull:
	docker pull lpendo/cantankerous-impurity


.PHONY: docker.run.local docker.run docker.start docker.bash

docker.run.local:
	docker run -di --name cantankerous-impurity \
	--mount source=precalc-coeffs,target=/cantankerous-impurity/precalc-coeffs \
	--mount type=bind,\
	source=$(shell pwd)/src,\
	target=/cantankerous-impurity/tremulous-petrichor/src \
	cantankerous-impurity:latest


docker.run:
	docker run -di --name cantankerous-impurity \
	--mount source=precalc-coeffs,target=/cantankerous-impurity/precalc-coeffs \
	--mount type=bind,\
	source=$(shell pwd)/src,\
	target=/cantankerous-impurity/tremulous-petrichor/src \
	lpendo/cantankerous-impurity:latest

docker.start:
	docker start cantankerous-impurity

docker.stop:
	docker stop cantankerous-impurity

docker.remove:
	@make docker.stop
	docker rm cantankerous-impurity

docker.bash:
	@make docker.run || make docker.start ||:
	docker exec -it cantankerous-impurity bash


.PHONY: docker.make.main.debug docker.run.main.debug

docker.make.main.debug:
	@make docker.run || make docker.start ||:
	docker exec -it cantankerous-impurity make main.debug

docker.run.main.debug:
	@make docker.run || make docker.start ||:
	docker exec -it cantankerous-impurity ./main.debug ||:

docker.main.debug:
	make docker.make.main.debug
	make docker.run.main.debug
	docker exec -it cantankerous-impurity make main.debug


.PHONY: CheckCC Specification

CheckCC:
# clang/gcc version must be greater than 10 to use c++20
ifneq (${CXXVER_GTEQ_10}, 1)
	$(error Compiler ${CXX} cannot implement required c++20 features)
endif

Specification:
	python3 ${SPEC_DIR}/SpecificationGeneration.py

Data_DEPS += ${SPEC_DIR}/SpecificationGeneration.py
${SPEC_DIR}/Loc.h : ${Data_DEPS}
	make Specification

OperatorSpec_DEPS := ${SPEC_DIR}/SpecificationGeneration.py
OperatorSpec_DEPS += ${SPEC_DIR}/SpecificationSupport.py
OperatorSpec_DEPS += ${SPEC_DIR}/SpecificationClasses.py
OperatorSpec_DEPS += ${SPEC_DIR}/OperatorSpecification.py
${OPERATORS_DIR}/Spec.h : ${OperatorSpec_DEPS}
	make Specification

BasisSpec_DEPS := ${SPEC_DIR}/SpecificationGeneration.py
BasisSpec_DEPS += ${SPEC_DIR}/SpecificationSupport.py
BasisSpec_DEPS += ${SPEC_DIR}/SpecificationClasses.py
BasisSpec_DEPS += ${SPEC_DIR}/BasisSupport.py
BasisSpec_DEPS += ${SPEC_DIR}/BasisSpecification.py
${BASIS_DIR}/SubBasisInfo.h : ${BasisSpec_DEPS}
	make Specification
${BASIS_DIR}/SuperBasisInfo.h : ${BasisSpec_DEPS}
	make Specification
${BASIS_DIR}/Spec.h : ${BasisSpec_DEPS}
	make Specification

CoeffStorage_DEPS := ${SPEC_DIR}/SpecificationGeneration.py
CoeffStorage_DEPS += ${SPEC_DIR}/SpecificationSupport.py
CoeffStorage_DEPS += ${SPEC_DIR}/SpecificationClasses.py
CoeffStorage_DEPS += ${SPEC_DIR}/OperatorSpecification.py
${COEFFS_DIR}/Storage.h: ${CoeffStorage_DEPS}
	make Specification

CoeffLoc_DEPS := ${SPEC_DIR}/SpecificationGeneration.py
CoeffLoc_DEPS += ${SPEC_DIR}/SpecificationSupport.py
CoeffLoc_DEPS += ${SPEC_DIR}/SpecificationClasses.py
${COEFFS_DIR}/Loc.h: ${CoeffLoc_DEPS}
	make Specification

BlochLoc_DEPS := ${SPEC_DIR}/SpecificationGeneration.py
BlochLoc_DEPS += ${SPEC_DIR}/SpecificationSupport.py
BlochLoc_DEPS += ${SPEC_DIR}/SpecificationClasses.py
${BLOCH_DIR}/Loc.h: ${BlochLoc_DEPS}
	make Specification

bloch.Gamble:
	python3 ${PRECALCULATION_DIR}/BlochCoeffExtraction.py




main_DEPS := main.cpp
main_DEPS += TremulousPetrichor.h
main_DEPS += ${PARAMS_DIR}/General.h ${PARAMS_DIR}/Physical.h
main_DEPS += ${PARAMS_DIR}/Symmetry.h ${PARAMS_DIR}/MathematicsSupport.h
main_DEPS += ${PARAMS_DIR}/StorageClasses.h

main_DEPS += ${RESULT_DIR}/Loc.h
main_DEPS += ${OPERATORS_DIR}/SolidAngle.h
main_DEPS += ${OPERATORS_DIR}/Decay.h
main_DEPS += ${OPERATORS_DIR}/Support.h
main_DEPS += ${OPERATORS_DIR}/Spec.h
main_DEPS += ${OPERATORS_DIR}/SpecSupport.h
main_DEPS += ${BASIS_DIR}/SubBasisInfo.h
main_DEPS += ${BASIS_DIR}/Classes.h
main_DEPS += ${OPERATORS_DIR}/DecaySupport.h
main_DEPS += ${BASIS_DIR}/SubBasis.h
main_DEPS += ${BASIS_DIR}/SuperBasisInfo.h
main_DEPS += ${BASIS_DIR}/SuperBasis.h
main_DEPS += ${BASIS_DIR}/Spec.h
main_DEPS += ${BASIS_DIR}/SpecSupport.h 
main_DEPS += ${OPERATORS_DIR}/Classes.h
main_DEPS += ${OPERATORS_DIR}/Constraints.h

main_DEPS += ${SQL_DIR}/Container.h ${SQL_DIR}/Interface.h

main_DEPS += ${COEFFS_DIR}/Loc.h
main_DEPS += ${COEFFS_DIR}/Spec.h
main_DEPS += ${COEFFS_DIR}/Container.h ${COEFFS_DIR}/Interface.h
main_DEPS += ${COEFFS_DIR}/Pair.h ${COEFFS_DIR}/Elem.h
main_DEPS += ${COEFFS_DIR}/Manager.h

main_DEPS += ${BLOCH_DIR}/Loc.h 
main_DEPS += ${BLOCH_DIR}/Container.h ${BLOCH_DIR}/Interface.h
main_DEPS += ${BLOCH_DIR}/Storage.h

main_DEPS += ${VIZ_DIR}/Interface.h

main_DEPS += ${INTEG_DIR}/Support.h
main_DEPS += ${INTEG_DIR}/SolidAngle/Support.h
main_DEPS += ${INTEG_DIR}/Radial/Support.h
main_DEPS += ${INTEG_DIR}/Calcs.h
main_DEPS += ${INTEG_DIR}/Radial/Core_Td.h
main_DEPS += ${INTEG_DIR}/Radial/Summation.h
main_DEPS += ${INTEG_DIR}/Summation.h

main_DEPS += ${PUNCTURE_DIR}/Integrands.h ${PUNCTURE_DIR}/Manager.h
main_DEPS += ${QUADRATURE_DIR}/Integrands.h ${QUADRATURE_DIR}/Manager.h
main_DEPS += ${CUBATURE_DIR}/Integrands.h ${CUBATURE_DIR}/Manager.h

main_DEPS += ${MATRIX_DIR}/Elem.h ${MATRIX_DIR}/Manager.h

main.debug: ${main_DEPS}
	make CheckCC
	${CXX} -ggdb -O0 ${CXXFLAGS} $(abspath $<) -l{cuba,sqlite3,petsc,slepc,m} -o $@

main.release: ${main_DEPS}
	make CheckCC
	${CXX} -O3 ${CXXFLAGS} $(abspath $<) -l{cuba,sqlite3,petsc,slepc,m} -o $@